"use strict";
var is_pc = false;
var fix_keyboard = true;
var statusBarShowerTimeout = null;
var newTargetSupported = true;

document.createElementOld = document.createElement;
/**
 * Creates a HTML DOM Node (like how the old createElement did it) with some more parameters.
 * @param {string} nodeType Node Name / Type
 * @param {string} [className] Optional string with classNames
 * @param {string} [innerHTML] Optional string with HTML code or text
 * @param {HTMLElement} [parentNode] A DOM node the node will automatically be appended to
 * @returns {HTMLElement} a DOM Node
 */
document.createElement = function (nodeType,className,innerHTML,parentNode) {
	var e = document.createElementOld(nodeType);
	if(className) {e.className=className;}
	if(innerHTML) {if(typeof(innerHTML) == "string") {e.innerHTML=innerHTML;} else if(typeof(innerHTML) == "object") {e.appendChild(innerHTML);}}
	if(parentNode) {parentNode.appendChild(e);}
	return e;
}
/**
 * Sets up the window variables for the Dialog
 */
function dialog_init() {
	window.dialog_frame = document.createElement('div','dialogFrame blockSN');
	if(system.api_version && system.api_version <= 18) {
		window.dialog_frame.className += " compat";
	}
	window.dialog_element = document.createElement('div','dialog');
	window.dialog_loader = document.createElement('canvas','dialog_loader');
	window.dialog_message = document.createElement('span','dialog_message');
	window.dialog_buttons = document.createElement('div','dialog_buttons');
	dialog_element.appendChild(dialog_loader);
	dialog_element.appendChild(dialog_message);
	dialog_element.appendChild(dialog_buttons);
	dialog_frame.appendChild(dialog_element);
	document.body.appendChild(dialog_frame);
	window.dialog_stack = [];
	window.showing_message = null;
	window.dialog_available = true;
	window.fix_dialog_size = function() {
		return dialog_message.style.maxHeight = "calc(80vh - "+(dialog_buttons.offsetHeight)+"px)";
	}
	if(window.system && window.system.orientationchangeEvents) {
		window.system.orientationchangeEvents.push(function() {window.setTimeout(fix_dialog_size,250);});
	}
}
/**
 * Sets up the window variables for the Snackbar
 */
function snackbar_init() {
	window.snackbar_element = document.createElement('div',"snackbar blockSN");
	window.snackbar_content = document.createElement('span',"snackbar_content");
	window.snackbar_buttons = document.createElement('div',"snackbar_buttons");
	snackbar_element.appendChild(snackbar_content);
	snackbar_element.appendChild(snackbar_buttons);
	document.body.appendChild(snackbar_element);
	window.snackbar_stack = [];
	window.showing_snackbar = null;
	window.last_snackbar_open = 0;
}
/**
 * Sets up the window variables for the Inline Cards
 */
function inlineCard_init() {
	window.inlineCard_element = document.createElement('div','cardParent blockSN',null,document.body);
	window.inlineCard_content = document.createElement('div','cardContent',null,inlineCard_element);
	var lastScroll = 0;
	window.inlineCard_content.addEventListener('scroll',function(e) {
		if(inlineCard_content.scrollTop > window.innerWidth/16*9 && inlineCard_content.scrollTop < lastScroll) {inlineCard_header.className = "cardHeader visible";} else {inlineCard_header.className = "cardHeader";}
		lastScroll = inlineCard_content.scrollTop;
	});
	window.inlineCard_imageContainer = document.createElement('div','cardImageContainer',null,inlineCard_content);
	window.inlineCard_image = document.createElement('img','cardImage',null,inlineCard_imageContainer);
	window.inlineCard_image.addEventListener('load',function() {if(window.inlineCard_image.src) {window.inlineCard_image.className = "cardImage loaded";}},window.supportsPassive ? {passive:true} : false);
	window.inlineCard_title = document.createElement('h1','cardTitle',null,inlineCard_content);
	window.inlineCard_text = document.createElement('span','cardText',null,inlineCard_content);
	window.inlineCard_header = document.createElement('div','cardHeader',null,inlineCard_element);
	window.inlineCard_headerTitle = document.createElement('h1','cardHeaderTitle',null,inlineCard_header);
	window.inlineCard_closebutton = new Button('small','&#xE5CD;',function() {if(window.showingInlineCard){showingInlineCard.close();}});
	window.inlineCard_closebutton.button.className += " cardCloseButton";
	window.inlineCard_closebutton.off = -30;
	inlineCard_header.appendChild(inlineCard_closebutton.button);
	window.inlineCard_deleteButton = new Button('small','&#xe92b;',function() {if(window.showingInlineCard){showingInlineCard.callbacks.leaveBehindPress();}});
	window.inlineCard_deleteButton.button.className += " cardCloseButton cardExtraButton";
	window.inlineCard_deleteButton.off = -30;
	inlineCard_header.appendChild(inlineCard_deleteButton.button);
	window.inlineCard_loaderE = document.createElement('canvas','card_loader',null,inlineCard_content);
	window.inlineCard_loader = new loader(window.inlineCard_loaderE);
	
	// Custom error image.
	window.inlineCard_error = document.createElement('div','inlineCard_error');
	window.inlineCard_error_position = document.createElement('div','image_positioning');
	window.inlineCard_error_img1 = document.createElement('img','message_error_img');
	window.inlineCard_error_img1.src = "img/tolsma_errorlogo.png";
	window.inlineCard_error_img2 = document.createElement('img','message_error_img2');
	window.inlineCard_error_img2.src = "img/tolsma_errorlogo_part.png";
	window.inlineCard_error_text = document.createElement('h2','message_error_text');
	if(window.lang && window.lang.translate) {
		window.inlineCard_error_text.innerHTML = lang.translate('unable_load_message');
	} else {
		window.inlineCard_error_text.innerHTML = "Unable to load message.<br>Please check your internet connection.";
	}
	
	inlineCard_error_position.appendChild(inlineCard_error_img1);
	inlineCard_error_position.appendChild(inlineCard_error_text);
	inlineCard_error.appendChild(inlineCard_error_position);
	inlineCard_error.appendChild(inlineCard_error_img2);
	inlineCard_element.appendChild(inlineCard_error);
	window.showingInlineCard = null;
}

/**
 * Creates an random string with numbers, uppercase letters and normal letters.
 * @param {number} [length] Length of the string. Default is 16
 * @returns {string}
 */
function randstr(length) {
	var out = "";
	while(out.length < (length || 16)) {
		var i = Math.floor(Math.random()*52);
		if(i < 26) {
			out += String.fromCharCode(i+65);
		} else {
			out += String.fromCharCode(i+71);
		}
	}
	return out;
}
/**
 * Inserts newNode after referenceNode in the same parent.
 * @param {HTMLElement} referenceNode 
 * @param {HTMLElement} newNode 
 */
function insertAfter(referenceNode, newNode) {
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
/**
 * Creates an instance of Dialog (must be called with new)
 * @param {string} type can be one of the following things:
 * 1. alert - a simple dialog with content, title and buttons
 * 2. loader - A simple dialog with optional content and title. It'll display a Loader within its content
 * 3. prompt - A more advanced dialog with one or more inputs. Content is required to be an Object[] with Object[]s containing the following fields:  
 * 	 type (_string_) input type,  
 *   [isPassword] (_boolean_) defaults to false
 * @param {(string|Object[])} content Can be one of a few variables
 * 1. string: pure innerHTML text
 * 2. Object (HTML DOM Node) uses appendChild
 * 3. Array with a combination of the above
 * @param {string} [title] String contained in a <h2> node
 * @param {Array} [buttons] Array with objects with the following fields:
 * * {text} (_string_) button text
 * * [filled] (_boolean_) defaults to false, if the background color should be filled
 * * [requiresResults] (_boolean_) For prompts, if the inputs need to pass their validation before closing.
 * * [action] (_function_) the function that should be executed when the button is pressed. Function parameter is {target:button}
 */
var Dialog = function(type,content,title,buttons) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Dialog': Please use the 'new' operator.");}
	if(!window.dialog_stack) {dialog_init();}
	
	
	this.type = type;
	this.title = title;
	this.content = content;
	this.loader = null;
	
	this.id = randstr(16);
	
	if(buttons == undefined) {
		this.buttons = [{text:"Sluit"}];
	} else {
		this.buttons = buttons;
	}
}
/**
 * Requests the current Dialog instance to be displayed on-screen. 
 * If there's another dialog already displayed, it'll be put in a call stack.
 * @returns {boolean} True if opening the current dialog was successfull, false if it's put in the call stack
 */
Dialog.prototype.open = function() {
	if(!window.dialog_stack) {throw("Program Execution Error: window.dialog_stack unavailable!");}
	if(window.showing_message !== null || !window.dialog_available) {
		dialog_stack[dialog_stack.length] = this;
		return false;
	}
	window.dialog_available = false;
	window.showing_message = this.id;
	function bindEvent(t,b, action) {
		b.addEvent(function(event) {t.handlePrompt(action);});
	}
	
	switch(this.type) {
		case 'alert':
			if(this.title) {
				dialog_message.innerHTML = "<h2>"+this.title+"</h2>";
				if(typeof(this.content) == "string") {
					dialog_message.innerHTML += this.content;
				} else if(typeof(this.content) == "object") {
					dialog_message.appendChild(this.content);
				}
			} else {
				if(typeof(this.content) == "string") {
					dialog_message.innerHTML = this.content;
				} else if(typeof(this.content) == "object") {
					dialog_message.innerHTML = "";
					dialog_message.appendChild(this.content);
				}
			}
			dialog_buttons.innerHTML = "";
			for(var i = 0; i < this.buttons.length; i++) {
				if(!this.buttons[i]) {continue;}
				var n = "small";
				if(this.buttons[i].filled) {n = "colored";}
				if(this.buttons[i].keepOpen) {
					var b = new Button(n,this.buttons[i].text,null,undefined,"rgba(0,0,0,0.25)");
				} else {
					var b = new Button(n,this.buttons[i].text,this.close.bind(this),undefined,"rgba(0,0,0,0.25)");
				}
				if(this.buttons[i].action) {
					b.addEvent(this.buttons[i].action);
				}
				dialog_buttons.appendChild(b.button);
				dialog_element.className="dialog hasButtons";
			}
		break;
		case 'loader':
			if(typeof(this.content) == "string") {
				dialog_message.innerHTML = this.content;
			} else if(typeof(this.content) == "object") {
				dialog_message.innerHTML = "";
				dialog_message.appendChild(this.content);
			}
			dialog_buttons.innerHTML = "";
			dialog_element.className="dialog fixedSize";
			dialog_message.className="dialog_message center showsLoader";
			dialog_loader.className="dialog_loader visible";
			this.loader = new loader(dialog_loader);
			this.loader.start();
		break;
		case 'prompt':
			if(this.title) {
				dialog_message.innerHTML = "<h2>"+this.title+"</h2>";
			}
			if(typeof(this.content) == "object") {
				this.inp = {};
				for(var i in this.content) {
					if(typeof(this.content[i]) === "string") {
						var t = document.createElement('span',null,this.content[i]);
						dialog_message.appendChild(t);
					} else {
						var settings = this.content[i];
						settings.required = true;
						this.inp[i] = new Input(settings);
						dialog_message.appendChild(this.inp[i].getElement());
					}
				}
			} else {
				var type = "text";
				if(this.isPassword) {type = "password";}
				this.inp = new Input({label:this.content,type:type});
				dialog_message.appendChild(this.inp.getElement());
			}
			dialog_buttons.innerHTML = "";
			for(var i = 0; i < this.buttons.length; i++) {
				if(!this.buttons[i]) {continue;}
				var n = "small";
				if(this.buttons[i].filled) {n = "colored";}
				var b = new Button(n,this.buttons[i].text,this.close.bind(this),'btn_ind_'+i,"rgba(0,0,0,0.25)");
				bindEvent(this,b,this.buttons[i].action);
				dialog_buttons.appendChild(b.button);
				dialog_element.className="dialog hasButtons";
			}
		break;
	}
	
	dialog_frame.style.display='block';
	window.setTimeout(function() {
		fix_dialog_size();
		dialog_frame.className += " visible";
		window.dialog_available = true;
	},33);
	return true;
}
/**
 * Requests the current dialog instance to be hidden  
 * Also stops any loaders, and **opens the next dialog in the call stack**
 * @param {event} event If the close function is called from a button. Used for prompt dialogs
 * @param {booean} [override] If it should force the dialog to be closed, even if the currently displayed dialog is not from this instance.
 * @returns {boolean} Returns false if force is false and the current dialog is not from this instance.
 */
Dialog.prototype.close = function(event,override) {
	// We should first check if this is the dialog that's being displayed.
	if(window.showing_message !== this.id && !override) {
		// If it's not, we should check our stack. If it's in the stack, we need to cancel it.
		console.log("lol, it ain't time!");
		var indx = window.dialog_stack.indexOf(this);
		console.log("Your index number is",indx)
		if(indx >= 0) {
			var new_stack = [];
			for(var i = 0; i < window.dialog_stack.length; i++) {
				if(i == indx) {continue;} // Skip the current item from the stack;
				new_stack[new_stack.length] = window.dialog_stack[i];
			}
			console.log('old stack',window.dialog_stack,'new stack',new_stack);
			window.dialog_stack = new_stack;
		}
		
		return false;
	}
	if(this.type == "prompt" && this.inp && event) {
		// check if this button requires results.
		var skip = true;
		var i = event.target.id.substr(8);
		if(this.buttons[i]) {
			if(this.buttons[i].requiresResults) {
				skip = false;
			}
		}
		if(!skip) {
			var valid = true;
			if(typeof(this.content) == "object") {
				for(var i in this.inp) {
					if(!this.inp[i].checkValidity()) {
						valid = false;
					}
					this.inp[i].blur();
				}
			} else {
				if(!this.inp.checkValidity()) {
					valid = false;
				}
				this.inp.blur();
			}
			if(!valid) {
				return false;
			}
		}
	}
	if(document.activeElement && document.activeElement.blur) {document.activeElement.blur();}
	dialog_frame.className = dialog_frame.className.replace(" visible","");
	if(this.type == "loader" && this.loader.stop) {this.loader.stop();}
	if(this.timer != undefined) {window.clearInterval(this.timer);}
	window.setTimeout(function() {
		dialog_frame.style.display='none';
		dialog_element.className = "dialog";
		dialog_message.className = "dialog_message";
		dialog_loader.className = "dialog_loader";
		window.showing_message = null;
		if(window.dialog_stack.length > 0) {
			var mainstack = dialog_stack[0];
			dialog_stack = dialog_stack.splice(1);
			mainstack.open();
		}
	},200);
	return true;
}
/**
 * Handles validity checking and optional action function of prompt dialogs
 * @param {function} action function called with _Object[]_ containing the names of inputs
 */
Dialog.prototype.handlePrompt = function(action) {
	var valid = true;
	if(typeof(this.content) == "object") {
		var data = {};
		for(var i in this.inp) {
			if(!this.inp[i].checkValidity(true)) {
				valid = false;
			}
			data[i] = this.inp[i].value;
		}
	} else {
		if(!this.inp.checkValidity(true)) {
			valid = false;
		}
		var data = this.inp.value;
	}
	if(!valid) {return;}
	if(data !== null && data !== undefined && action) {
		action(data);
	}
}
/**
 * Sets the content for alert dialogs
 * @param {(string|Element)} content 
 */
Dialog.prototype.setContent = function(content) {
	this.content = content;
	if(window.showing_message == this.id) {
		if(typeof(this.content) == "string") {
			dialog_message.innerHTML = this.content;
		} else if(typeof(this.content) == "object") {
			dialog_message.innerHTML = "";
			dialog_message.appendChild(this.content);
		}
	}
}
/**
 * Creates and instance of Button for a (optionaly) inline button with animations
 * @param {string} type Button type (added to classText). 
 * * small - Special type for smaller buttons
 * * filled - Can be combined with colored, adds a overflow:hidden
 * * colored - Can be used with colored, adds another color
 * @param {string} content A string with the content of the button
 * @param {function} clickAction Function that is called when the button is pressed
 * @param {string} [id] Button ID
 * @param {string} [color] Button press shader color
 * @param {string} [cssText] Additional CSS text for the button
 */
var Button = function(type,content,clickAction,id,color,cssText) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Button': Please use the 'new' operator.");}
	this.type = type;
	this.content = content;
	this.clickAction = [clickAction];
	this.id = id;
	this.cssText = cssText;
	if(!color) {
		this.color = "rgba(200,200,200,0.5)";
	} else {
		this.color = color;
	}
	this.off = -200;
	
	this.button = document.createElement('span');
	this.button.className = "button "+type;
	if(this.cssText) {this.button.style.cssText += this.cssText;}
	if(this.id) {this.button.id = this.id;}
	if(this.type != "small") {this.button.style.overflow='hidden';}
	
	if(is_pc) {
		this.button.addEventListener('mousedown',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
		this.button.addEventListener('mouseup',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		this.button.addEventListener('touchstart',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
		this.button.addEventListener('touchend',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
	}
	
	this.buttonShade = document.createElement('div','buttonShade');
	this.buttonShade.style.backgroundColor = this.color;
	if(this.type == "small") {
	//	this.buttonShade.style.backgroundImage = "-webkit-radial-gradient(50px at center, "+this.color+" 99%, transparent 100%) radial-gradient(50px at center, "+this.color+" 99%, transparent 100%)";
		this.buttonShade.style.webkitTransition = "150ms ease-out -webkit-transform, 100ms linear opacity";
		this.buttonShade.style.transition = "150ms ease-out transform, 100ms linear opacity";
	} else {
		this.buttonShade.style.webkitTransition = "2000ms ease-out -webkit-transform, 100ms linear opacity";
		this.buttonShade.style.transition = "2000ms ease-out transform, 100ms linear opacity";
	}
	this.button.appendChild(this.buttonShade);
	
	this.contentSpan = document.createElement('span');
	this.contentSpan.innerHTML = this.content;
	this.contentSpan.style.zIndex='2';
	this.contentSpan.style.position='relative';
	
	this.button.appendChild(this.contentSpan); 
	this.isTouching = false;
	
}
/**
 * Internal handler for pressing the button
 * @param {event} event 
 */
Button.prototype.handleTouch = function(event) {
	if(event.type == "mousedown" || event.type == "touchstart") {
		this.isTouching = true;
		this.sTouch = performance.now();
		if(this.type == "small") {
			var p = this.button.getBoundingClientRect();
			this.px = p.width/2;
			this.py = p.height/2;
		} else {
			if(event.touches) {
				this.px = event.touches[0].clientX;
				this.py = event.touches[0].clientY;
				var p = this.button.getBoundingClientRect();
				this.px -= p.left;
				this.py -= p.top;
			} else {
				this.px = event.layerX;
				this.py = event.layerY;
			}
		}
		window.setTimeout(this.handleHold.bind(this),100);
	} else if(event.type == "mouseup" || event.type == "touchend") {
		this.isTouching = false;
		this.buttonShade.style.opacity = "0";
		if(performance.now() - this.sTouch < 150) {
			this.handleRelease(); // clicking
		} else {
			window.setTimeout(this.handleRelease.bind(this),150); // Holding
		}
	}
}
/**
 * Adds a function to the button. Can be called externally too.
 * @param {function} action Function that is called when the button is pressed.
 */
Button.prototype.addEvent = function(action) {
	this.clickAction[this.clickAction.length] = action;
}
/**
 * Internal function for button pressing
 * @param {event} event 
 */
Button.prototype.handleHold = function(event) {
	if(this.isTouching) {
		if(this.type != "small") {
			this.buttonShade.style.webkitTransition = "2000ms ease-out -webkit-transform, 100ms linear opacity";
			this.buttonShade.style.transition = "2000ms ease-out transform, 100ms linear opacity";
			this.buttonShade.style.webkitTransform = "scale(0.5)";
			this.buttonShade.style.transform = "scale(0.5)";
		} else {
			this.buttonShade.style.webkitTransition = "250ms ease-out -webkit-transform, 100ms linear opacity";
			this.buttonShade.style.transition = "250ms ease-out transform, 100ms linear opacity";
			this.buttonShade.style.webkitTransform = "scale(1)";
			this.buttonShade.style.transform = "scale(1)";
		}
		this.buttonShade.style.top = (this.off+this.py)+"px";
		this.buttonShade.style.left = (this.off+this.px)+"px";
		this.buttonShade.style.opacity = "1.0";
	}
}
/**
 * Internal function for releasing the button press
 * @param {event} event 
 */
Button.prototype.handleRelease = function(event) {
	if(!this.isTouching) {
		if(performance.now() - this.sTouch <= 150) { // Tapped / clicked
			this.buttonShade.style.webkitTransition = "150ms ease-out -webkit-transform, 100ms linear opacity";
			this.buttonShade.style.transition = "150ms ease-out transform, 100ms linear opacity";
			this.buttonShade.style.top = (this.off+this.py)+"px";
			this.buttonShade.style.left = (this.off+this.px)+"px";
			this.buttonShade.style.webkitTransform = "scale(1)";
			this.buttonShade.style.transform = "scale(1)";
			this.buttonShade.style.opacity = "1.0";
			this.waitUntilFinish();
			var bs = this.buttonShade;
			window.setTimeout(function() {bs.style.opacity='0';},250);
			window.setTimeout(this.handleRelease.bind(this),350);
		} else {
			this.buttonShade.style.webkitTransition = "1ms ease-out -webkit-transform, 100ms linear opacity";
			this.buttonShade.style.transition = "1ms ease-out transform, 100ms linear opacity";
			this.buttonShade.style.webkitTransform = "scale(0.01)";
			this.buttonShade.style.transform = "scale(0.01)";
			this.buttonShade.style.opacity = "0";
		}
	}
}
/**
 * Internal function that delays button action calls
 * @param {event} [event] 
 */
Button.prototype.waitUntilFinish = function(event) {
	window.setTimeout(this.useAction.bind(this),150);
}
/**
 * Internal function that uses all functions assigned to the button
 */
Button.prototype.useAction = function() {
	for(var x = 0; x < this.clickAction.length; x++) {
		if(typeof(this.clickAction[x]) == "function") {
			this.clickAction[x]({target:this.button});
		}
	}
}

/**
 * Creates an instance of Slider, which is a horizontal slider, like <input type='range'>
 * @param {number} min Minimum value (left side of slider)
 * @param {number} max Maximum value (right side of slider)
 * @param {number} value Current value
 * @param {number} step Steps taken by slider. Slider binds to these steps when the user lets go
 * @param {number} width With of the slider element
 */
var Slider = function(min,max,value,step,width) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Slider': Please use the 'new' operator.");}
	// First check a few things!
	if(min >= max) {throw("Argument Error: Min is equal or greater then max!");}
	
	this.min = min || 0;
	this.max = max || 100;
	this.value = value || this.min;
	if(this.value > this.max) {this.value = this.max;}
	else if(this.value < this.min) {this.value = this.min;}
	this.step = step || 1;
	this.sliding = false;
	this.events = {};
	
	
	this.width = width || 300;
	this.padding = 10;
	
	this.element = document.createElement('span',"slider blockSN");
	this.element.style.width=this.width+"px";
	this.inner_slider = document.createElement('div');
	this.inner_slider.className="inner_slider";
	this.inner_slider.style.width = (this.width-this.padding*2)+"px";
	this.slidingProgress = document.createElement('div',"sliding_progress");
	this.inner_slider.appendChild(this.slidingProgress);
	this.element.appendChild(this.inner_slider);
	this.slidingElement = document.createElement('div',"slidingElement");
	this.inner_slider.appendChild(this.slidingElement);
	
	this.valuespan = document.createElement('span');
	this.valuespan.innerHTML = this.value;
	
	if(this.value) {
		var range = this.max-this.min;
		var val = this.value-this.min;
		var proc = val / range;
		this.slidingElement.style.left = ((this.width-this.padding*2) * proc)+"px";
		this.slidingProgress.style.width = ((this.width-this.padding*2) * proc)+"px";
	}
	
	if(is_pc) {
		this.element.addEventListener('mousedown',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
		this.element.addEventListener('mouseup',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
		this.element.addEventListener('mousemove',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		this.element.addEventListener('touchstart',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
		this.element.addEventListener('touchend',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
		this.element.addEventListener('touchmove',this.handleMouse.bind(this),window.supportsPassive ? {passive:true} : false);
	}
}
/**
 * External function to add event listeners to certain events.  
 * Current supported event names are:
 * 1. input
 * 2. change
 * @param {string} name 
 * @param {function} func 
 */
Slider.prototype.addEventListener = function(name,func) {
	this.events[name] = func;
}
/**
 * Internal function to get the location of the cursor compared to the element
 * @param {event} event Touch event
 * @param {HTMLElement} target Node
 */
Slider.prototype.getRelativePosition = function(event,target) {
	var p = target.getBoundingClientRect();
	if(event.touches) {
		var x = event.touches[0].clientX;
		var y = event.touches[0].clientY;
		
	} else {
		var x = event.clientX;
		var y = event.clientY;
		
	}
	x -= p.left;
	y -= p.top;
	return {x:x,y:y};
}
/**
 * Internal function that handle mouse / touch movement, presses etc
 * @param {event} event 
 */
Slider.prototype.handleMouse = function(event) {
	if(event.type == "mousedown" || event.type == "touchstart") {
		if(cordova.platformId == 'android' && system.api_version <= 19) {event.preventDefault();}
		this.sliding = true;
		this.slidingElement.className = "slidingElement sliding";
		var pos = this.getRelativePosition(event,this.inner_slider);
		if(pos.x > this.width-this.padding*2) {
			pos.x = this.width-this.padding*2;
		} else if(pos.x < 0) {
			pos.x = 0;
		}
		var nv = Math.round(this.calculateValue(pos.x) / this.step) * this.step;
		if(nv != this.value) {
			this.value = nv;
			if(this.events['input']) {this.events['input'](this.value);}
		}
		this.slidingElement.style.left = pos.x+"px";
		this.slidingProgress.style.width = pos.x+"px";
	} else if(event.type == "mouseup" || event.type == "touchend") {
		this.sliding = false;
		this.slidingElement.className = "slidingElement";
		if(this.events['change']) {this.events['change'](this.value);}
		// Now get the prec% of our value and apply it to the slider
		var range = this.max - this.min;
		var prec = (this.value - this.min) / range;
		
		this.slidingElement.style.left = ((this.width-this.padding*2) * prec)+"px";
		this.slidingProgress.style.width = ((this.width-this.padding*2) * prec)+"px";
	} else if(event.type == "mousemove" || event.type == "touchmove") {
		if(this.sliding) {
			var pos = this.getRelativePosition(event,this.inner_slider);
			if(pos.x > this.width-this.padding*2) {
				pos.x = this.width-this.padding*2;
			} else if(pos.x < 0) {
				pos.x = 0;
			}
			var nv = Math.round(this.calculateValue(pos.x) / this.step) * this.step;
			if(nv != this.value) {
				this.value = nv;
				this.valuespan.innerHTML = this.value;
				if(this.events['input']) {this.events['input'](this.value);}
			}
			this.slidingElement.style.left = pos.x+"px";
			this.slidingProgress.style.width = pos.x+"px";
		}
	}
}
/**
 * Internal function that calculates the value based upon the location in pixels
 * @param {number} px Pixels from the left of the slider
 * @returns {number} The value (min + range * current position %)
 */
Slider.prototype.calculateValue = function(px) {
	var prec = px / (this.width-this.padding*2);
	var range = this.max - this.min;
	return this.min + (range * prec);
}
/**
 * Function that sets the min, max and current value of the slider
 * @param {Object[]} data 
 * @param {number} data.min 
 * @param {number} data.max 
 * @param {number} data.value 
 */
Slider.prototype.setValues = function(data) {
	if(this.sliding) {return;}
	if(data.min != undefined) {this.min = data.min;}
	if(data.max != undefined) {this.max = data.max;}
	if(data.value != undefined) {this.value = data.value;}
	
	if(typeof(this.value) == "number") {
		this.valuespan.innerHTML = this.value;
		var range = this.max-this.min;
		var val = this.value-this.min;
		var proc = val / range;
		this.slidingElement.style.left = ((this.width-this.padding*2) * proc)+"px";
		this.slidingProgress.style.width = ((this.width-this.padding*2) * proc)+"px";
	}
}

/**
 * Creates an instance of Checkbox, which is similar to <input type='checkbox'>
 * @param {boolean} value State of the checkbox (checked or not)
 * @param {string} name Name of the checkbox (internal)
 * @param {boolean} [noAction] if the checkbox should be pressable
 */
var Checkbox = function(value,name,noAction) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Checkbox': Please use the 'new' operator.");}
	this.inpcb = null;
	this.name = name;
	this.value = value;
	this.disabled = noAction == true ? true : false;
	this.element = document.createElement('div',"checkbox");
	if(this.value) {
		this.element.className += " checked";
	}
	this.element.setAttribute('name',this.name);

	if(is_pc) {
		this.element.addEventListener('click',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		this.element.addEventListener('click',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
	}
	this.innercb = document.createElement('div',"innercb");
	this.innercb.innerHTML = "&#xe876;";
	this.element.appendChild(this.innercb);
	
	this.circleElement = document.createElement('div',"checkboxCircle");
	this.element.appendChild(this.circleElement);
}
/**
 * Returns the DOM Element containing the checkbox
 * @returns {HTMLElement}
 */
Checkbox.prototype.getElement = function() {
	return this.element;
}
/**
 * External function to add event listeners to certain events.  
 * Current supported event names are:
 * 1. input
 * 2. change
 * 
 * Currently, only 1 callback is supported at the same time.
 * @param {string} type Name of the event
 * @param {function} callback function called by event
 */
Checkbox.prototype.addEventListener = function(type,callback) {
	switch(type) {
		case 'change': case 'input':
			this.inpcb = callback;
		break;
	}
}
Checkbox.prototype.setAttribute = function(name,value) {
	this.element.setAttribute(name,value);
	this[name] = value;
}
Checkbox.prototype.getAttribute = function(name) {
	return this.element.getAttribute(name) || this[name];
}
/**
 * Internal function handling pressing the checkbox
 * @param {event} event 
 */
Checkbox.prototype.handleClick = function(event,force) {
	if(this.disabled && !force) {return;}
	this.value = !this.value;
	if(this.inpcb) {
		if(this.inpcb({target:this.element,value:this.value,type:'change'}) === false) {
			// The function returned false. This means we gotta cancel the check.
			this.value = !this.value;
			return;
		}
	}
	if(this.value) {
		this.element.className = "checkbox checked";
	} else {
		this.element.className = "checkbox";
	}
	if(event !== null) {
		this.circleElement.className= "checkboxCircle active";
		window.setTimeout(this.endCircle.bind(this),150);
	}
}
Checkbox.prototype.toggle = function(withEvent) {
	this.value = !this.value;
	if(this.value) {
		this.element.className = "checkbox checked";
	} else {
		this.element.className = "checkbox";
	}
	if(withEvent) {
		if(this.inpcb) {
			if(this.inpcb({target:this.element,value:this.value,type:'change'}) === false) {
				// The function returned false. This means we gotta cancel the check.
				this.value = !this.value;
				return;
			}
		}
	}
}
/**
 * Internal function handling animation
 * @param {event} event 
 */
Checkbox.prototype.endCircle = function(event) {
	var circleElement = this.circleElement;
	circleElement.style.opacity = 0.0;
	window.setTimeout(function() {
		circleElement.className = "checkboxCircle";
		window.setTimeout(function() {
			circleElement.style.opacity = 0.25;
		},250);
	},100);
}
	
/**
 * Creates a new instance of one or more Radio checkbox(es). Similar to <input type='radio' name='#name'>
 * @param {string} name Name of the combination of radio boxes
 */
var Radio = function(name) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Radio': Please use the 'new' operator.");}
	this.name = name;
	this.radios = [];
	this.radiocircles = [];
	this.radiostates = [];
	this.lasti = 0;
	this.events = {
		change: null,
		input: null
	};
}
/**
 * Adds a new radio to the stack.
 * @param {boolean} [checked] Wether this radio is checked or not. Defaults to false.
 * @returns {HTMLElement} HTML Dom Node of the radio button
 */
Radio.prototype.addRadioButton = function(checked) {
	var i = this.radios.length;
	this.radios[i] = document.createElement('div','radioButton');
	this.radios[i].setAttribute('number',i);
	this.radios[i].addEventListener('click',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
	if(checked) {
		this.radios[i].className += " active";
	}
	var bg = document.createElement('div',"radiobg");
	this.radios[i].appendChild(bg);
	
	this.radiocircles[i] = document.createElement('div',"radioCircle");
	this.radios[i].appendChild(this.radiocircles[i]);
	
	this.radiostates[i] = checked;
	return this.radios[i];
}
/**
 * Internal function handling the checked state of the radio button
 * @param {event} event 
 */
Radio.prototype.handleClick = function(event) {
	this.lasti = parseInt(event.target.getAttribute('number'));
//	console.log(event,this.lasti);
	this.uncheckAll();
	this.radiostates[this.lasti] = true;
	if(this.radiostates[this.lasti]) {
		this.radios[this.lasti].className = "radioButton active";
	} else {
		this.radios[this.lasti].className = "radioButton";
	}
	this.radiocircles[this.lasti].className= "radioCircle active";
	if(this.events.change) {this.events.change({target:event.target,index:this.lasti});}
	if(this.events.input) {this.events.input({target:event.target,index:this.lasti});}
	window.setTimeout(this.endCircle.bind(this),150);
}
/**
 * Internal function handling the animation
 * @param {event} event 
 */
Radio.prototype.endCircle = function(event) {
	var circleElement = this.radiocircles[this.lasti];
	circleElement.style.opacity = 0.0;
	window.setTimeout(function() {
		circleElement.className = "radioCircle";
		window.setTimeout(function() {
			circleElement.style.opacity = 0.25;
		},250);
	},100);
}
/**
 * Function to clear the currently checked radios
 */
Radio.prototype.uncheckAll = function() {
	for(var i = 0; i < this.radios.length; i++) {
		this.radiostates[i] = false;
		this.radios[i].className = "radioButton";
	}
}
/**
 * Function that checks a specific radio button
 * @param {HTMLElement} element Node of the radio button that should be checked
 */
Radio.prototype.checkRadio = function(element) {
	this.lasti = this.radios.indexOf(element);
	this.uncheckAll();
	this.radiostates[this.lasti] = true;
	if(this.radiostates[this.lasti]) {
		this.radios[this.lasti].className = "radioButton active";
	} else {
		this.radios[this.lasti].className = "radioButton";
	}
}
/**
 * Function to add events. Not finished yet.
 * @param {HTMLElement} radio 
 * @param {string} type 
 * @param {function} callback 
 */
Radio.prototype.addEventListener = function(type,callback) {
	this.events[type] = callback;
}

/**
 * Creates a new instance of a Switch. This is similar to Checkboxes.
 * @param {boolean} state If the switch should begin active
 */
var Switch = function(state) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Switch': Please use the 'new' operator.");}
	this.state = state;
	this.touching = false;
	this.slided = false;
	this.touchX = null;
	this.lastX = null;
	this.element = document.createElement('div',"switchContainer");
	this.switch = document.createElement('div',"switch",null,this.element);
	if(this.state) {this.switch.className += " active";}
	this.element.addEventListener('touchstart',this.handleEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	this.element.addEventListener('touchend',this.handleEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	this.element.addEventListener('touchmove',this.handleEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	this.switcher = document.createElement('div','switcher',false,this.switch);
}
/**
 * Returns the DOM Element containing the checkbox
 * @returns {HTMLElement}
 */
Switch.prototype.getElement = function() {
	return this.element;
}
/**
 * Sets the checked or not checked state of the switch
 * @param {boolean} state 
 */
Switch.prototype.setState = function(state) {
	this.state = state;
	this.switch.className = "switch";
	if(this.state) {this.switch.className += " active";}
}
/**
 * Internal function handling mouse and touch events
 * @param {event} event 
 */
Switch.prototype.handleEvent = function(event) {
	console.log(event);
	switch(event.type) {
		case 'touchstart':
			if(cordova.platformId == 'android' && system.api_version <= 19) {event.preventDefault();}
			this.touching = true;
			this.slided = false;
			this.touchX = event.touches[0].clientX;
		break;
		case 'touchmove':
			this.slided = true;
			this.lastX = event.touches[0].clientX;
		break;
		case 'touchend':
			if(!this.touching) {return;}
			if(!this.slided) {// We tapped
				this.switchState();
			} else { // We slided (I think)
				var Delta = this.lastX - this.touchX;
				if(Delta < 0) {
					// We moved to the left side.
					if(this.state) { // Only change if we're currently set to TRUE.
						this.setState(false);
						if(this.cb) {this.cb(this.state);}
					}
				} else if(Delta > 0) {
					// We moved to the right side.
					if(!this.state) { // Only change if we're currently set to FALSE.
						this.setState(true);
						if(this.cb) {this.cb(this.state);}
					}
				}
			}
			
			this.touching = false;
			this.slided = false;
			this.touchX = null;
			this.lastX = null;
		break;
	}
}
/**
 * Toggle the switch on or off
 */
Switch.prototype.switchState = function() {
	this.setState(!this.state);
	if(this.cb) {this.cb(this.state);}
	return this.state;
}
/**
 * Adds a event handler for change or input events. Currently only one function is supported.
 * @param {string} type 
 * @param {function} callback 
 */
Switch.prototype.addEventListener = function(type,callback) {
	switch(type) {
		case 'change': case 'input':
			this.cb = callback;
		break;
	}
}

/**
 * Creates a new instance of a SideNav. These are menu's on the left side of the screen.
 */
var SideNav = function() {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'SideNav': Please use the 'new' operator.");}
	this.element = document.createElement('div',"sideNav");
	this.header = document.createElement('div','sidenav_header',false,this.element);
	this.listElement = document.createElement('div','sideNav_list',false,this.element);
	this.closeButton = new Button("small","&#xe5c4;",this.toggle.bind(this));
	this.closeButton.button.className += " sidenav_close";
	this.header.appendChild(this.closeButton.button);
	this.title_e = document.createElement('span',"sidenav_title",false,this.header);
	this.backgroundBlocker = document.createElement('div','sideNav_blocker');
	var t = this;
	var moved = false;
	this.backgroundBlocker.addEventListener('touchstart',function() {moved=false;},window.supportsPassive ? {passive:true} : false);
	this.backgroundBlocker.addEventListener('touchmove',function() {moved=true;},window.supportsPassive ? {passive:true} : false);
	this.backgroundBlocker.addEventListener('touchend',function() {if(!moved){t.toggle(false);}},window.supportsPassive ? {passive:true} : false);
	this.open = false;
	this.title = "";
	this.background = null;
	this.icon = null,
	this.list = [];
	this.isSliding = false;
	this.sx = null;
	this.sx2 = null;
	this.lastToggled = 0;
	if(window.system && window.system.api_version <= 19) {return;}
	if(is_pc) {
		document.body.addEventListener('mousedown',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
		document.body.addEventListener('mouseup',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
		document.body.addEventListener('mousemove',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		document.body.addEventListener('touchstart',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
		document.body.addEventListener('touchend',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
		document.body.addEventListener('touchmove',this.cursorEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	}
	
	
}
/**
 * Adds a special button on the top right section of the SideNav.
 * @param {string} icon String for the text or icon of the button
 * @param {function} callback Function called when this button is pressed
 */
SideNav.prototype.createAccountButton = function(icon,callback) {
	this.accountButton = new Button("small",icon,callback);
	this.accountButton.off = -30;
	this.accountButton.button.className += " accountButton";
	this.header.appendChild(this.accountButton.button)
}
/**
 * Hides the account button
 */
SideNav.prototype.hideAccountButton = function() {
	if(this.accountButton) {
		this.accountButton.button.style.display = "none";
	}
}
/**
 * Shows the account button
 */
SideNav.prototype.showAccountButton = function() {
	if(this.accountButton) {
		this.accountButton.button.style.display = "";
	}
}
/**
 * Sets the title of the SideNav (top right)
 * @param {string} title 
 */
SideNav.prototype.setTitle = function(title) {
	this.title = title;
	if(typeof(this.title) == "object") {
		this.title_e.innerHTML = this.title[0] + "<br><small>"+this.title[1]+"</small>";
	} else {
		this.title_e.innerHTML = this.title;
	}
}
/**
 * Sets the image URL of the background
 * @param {string} backgroundaddress URL
 */
SideNav.prototype.setBackground = function(backgroundaddress) {
	this.background = backgroundaddress;
	this.header.style.backgroundImage = "src(\""+this.background+"\")";
}
/**
 * Sets wether to show a X close button at the top left of the SideNav
 * @param {boolean} state 
 */
SideNav.prototype.showCloseButton = function(state) {
	if(state) {
		this.closeButton.button.style.display="inline-block";
	} else {
		this.closeButton.button.style.display="none";
	}
}
/**
 * Creates a new SideNav menu item and stores it in its list
 * @param {string} title Title of the item
 * @param {string} icon Icon left of the item
 * @param {function} callback Function called when item is pressed
 * @param {string} [secondicon] Right icon
 * @param {boolean} [isInactive] Wether it's active or inactive (true for inactive)
 * @param {function} [inactiveCallback] Function called when the item is pressed while isInactive is true
 * @param {number} [insertAfter] Not used yet.
 * @returns {number} The ID of the item.
 */
SideNav.prototype.addItem = function(title,icon,callback,secondicon,isInactive,inactiveCallback,insertAfter) {
	var i = this.list.length;
	this.list[i] = {
		title: title,
		icon: icon,
		callback: callback,
		secondIcon: secondicon,
		alerts: 0,
		visible: true,
		active: !isInactive,
		inactiveCallback: inactiveCallback,
		element: document.createElement('div',"sidenav_listitem"),
		title_element: document.createElement('span',"title"),
		icon_element: document.createElement('span',"icon"),
		alerts_icons: document.createElement('span',"alerts")
	};
	if(!this.list[i].active) {this.list[i].element.className += " disabled";}
	this.list[i].element.setAttribute('itemid',i);
	this.list[i].title_element.innerHTML = this.list[i].title;
	this.list[i].icon_element.innerHTML = this.list[i].icon;
	if(is_pc) {
		this.list[i].element.addEventListener('click',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		this.list[i].element.addEventListener('touchstart',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
		this.list[i].element.addEventListener('touchmove',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
		this.list[i].element.addEventListener('touchend',this.handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
	}
	this.list[i].element.appendChild(this.list[i].title_element);
	this.list[i].element.appendChild(this.list[i].icon_element);
	this.list[i].element.appendChild(this.list[i].alerts_icons);
	this.listElement.appendChild(this.list[i].element);
	
	return i;
}
/**
 * Sets the title of a specific item 
 * @param {number} item ID of the item
 * @param {string} title Title string
 */
SideNav.prototype.updateItemTitle = function(item,title) {
	if(item == undefined) {return false;}
	if(!this.list[item]) {return false;}
	this.list[item].title = title;
	this.list[item].title_element.innerHTML = this.list[item].title;
}
/**
 * Sets the visibility of a specific item
 * @param {number} item ID of the item
 * @param {boolean} visibility Visible or not
 */
SideNav.prototype.setItemVisibility = function(item,visibility) {
	if(item == undefined) {return false;}
	if(!this.list[item]) {return false;}
	if(visibility == undefined) {
		this.list[item].visible = !this.list[item].visible;
	} else {
		this.list[item].visible = visibility
	}
	
	if(this.list[item].visible) {
		this.list[item].element.style.display="block";
	} else {
		this.list[item].element.style.display="none";
	}
}
/**
 * Removes a item permenantly
 * @param {number} i ID of the item
 */
SideNav.prototype.removeItem = function(i) {
	if(i == undefined) {return false;}
	if(!this.list[i]) {return false;}
	this.listElement.removeChild(this.list[i].element);
	delete(this.list[i]);
}
/**
 * Shows a red circle at the right side of the item with _alerts_ as content. If the content == 0, no circle will be displayed
 * @param {number} item ID of the item
 * @param {number} alerts Number of alerts in the circle to be displayed
 */
SideNav.prototype.setListAlerts = function(item,alerts) {
	if(item == undefined) {return false;}
	if(!this.list[item]) {return false;}
	this.list[item].alerts = alerts;
	this.list[item].alerts_icons.innerHTML = this.list[item].alerts;
	if(alerts > 0) {
		this.list[item].alerts_icons.className = "alerts visible";
	} else {
		this.list[item].alerts_icons.className = "alerts";
	}
}
/**
 * Internal function handling clicks
 * @param {event} event 
 */
SideNav.prototype.handleClick = function(event) {
	if(event.type == "touchstart") {
		//if(cordova.platformId == 'android') {event.preventDefault();} // I mean, only detecing a single move is enough so I guess it's fine.
		this.clickingOnItem = this._getListItem(event);
	}
	if(event.type == "touchmove") {
		this.clickingOnItem = null;
	}
	if(event.type == "touchend" || event.type == "click") {
		var e = this._getListItem(event);
		if(e != this.clickingOnItem && !is_pc) {return;}
		var i = e.getAttribute('itemid');
		if(!this.list[i]) {return;}
		if(this.list[i].active) {
			this.list[i].callback(event);
		} else {
			if(this.list[i].inactiveCallback) {
				this.list[i].inactiveCallback(event);
			}
		}
		this.sx = null;
		this.sx2 = null;
		this.isSliding = false;
	}
}
/**
 * Toggles an item active or inactive from the list
 * @param {number} i ID of the list item
 * @param {boolean} state 
 */
SideNav.prototype.setActiveList = function(i,state) {
	if(i == undefined || state == undefined) {return false;}
	if(!this.list[i] || typeof(state) != "boolean") {return false;}
	this.list[i].active = state;
	if(!this.list[i].active) {
		this.list[i].element.className = "sidenav_listitem disabled";
		this.setListAlerts(i,0);
	} else {
		this.list[i].element.className = "sidenav_listitem";
	}
}
/**
 * Internal function that returns the ID of a list item that was clicked upon
 * @param {event} event 
 */
SideNav.prototype._getListItem = function(event) {
	var e = event.target;
	while(e.getAttribute('itemid') == null && e != document.body) {
		e = e.parentNode;
	}
	if(e == document.body) {return null;}
	return e;
}
/**
 * Internal function to check if the user interacted with the SideNav
 * @param {event} event 
 * @returns {boolean} Whether the user clicked on the SideNav
 */
SideNav.prototype._isAboveSN = function(event) {
	if(event.path) {
		if(event.path.indexOf(this.element) == -1) {
			// No sidenav here...
			return false;
		}
	} else {
		// Fallback check
		var e = event.target;
		while(e != this.element && e != document.body) {e = e.parentNode;}
		if(e == document.body) {
			// Didn't click on the sidenav.
			return false;
		}
	}
	return true;
}
/**
 * Internal function that checks if the user interacted with an item that blocks SideNav interaction
 * @param {event} event 
 */
SideNav.prototype._aboveBI = function(event) {
	var e = event.target;
	while(e.className.indexOf("blockSN") == -1 && e != document.body) {e = e.parentNode;}
	if(e == document.body) {return false;}
	return true;
}
/**
 * Function that handles mouse and touch events
 * @param {event} event 
 */
SideNav.prototype.cursorEvent = function(event) {
	switch(event.type) {
		case 'mousedown':
		case 'touchstart':
			if(this._aboveBI(event)) {return;}
			//if(cordova.platformId == 'android') {event.preventDefault();}
			var x = 0;
			if(is_pc) {
				x = event.clientX;
			} else {
				x = event.touches[0].clientX;
			}
			if((window.innerWidth/10 >= x && !this.open) || (this.open)) {
				this.sx = x;
				this.sx2 = x;
			}
		break;
		case 'mousemove':
		case 'touchmove':
			if(this.sx === null) {return;}
			this.isSliding = true;
			var x = 0;
			if(is_pc) {
				x = event.clientX;
			} else {
				x = event.touches[0].clientX;
			}
			this.sx2 = x;
			if(!this.open && this.sx2 - this.sx > 280) {
				this.sx2 = this.sx+280;
			} else if(this.open && this.sx2 - this.sx > 0) {
				this.sx2 = this.sx;
			}
			this._updateSNPos();
		break;
		case 'mouseup':
		case 'touchend':
			if(!this.isSliding || this.sx === null) {this.sx = null; this.sx2 = null; this.isSliding = false;return;}
			this.element.removeAttribute('style');
			this.element.removeAttribute('sliding');
			if(!this.open) {this.backgroundBlocker.removeAttribute('style');}
			if(Math.abs(this.sx2 - this.sx) < 20) {this.sx = null; this.sx2 = null; this.isSliding = false;return;} // We haven't really moved.
			this.isSliding = false;
			if(this.sx2 - this.sx > 60) {
				this.toggle(true);
			} else {
				if(this.open) {
				//	console.log("forcing toggle close");
					this.toggle(false);
				}
			}
			this.sx = null;
			this.sx2 = null;
		break;
	}
}
/**
 * Toggles the SideNav open or closed
 * @param {boolean} [force] if given, manually set the state instead of toggling
 */
SideNav.prototype.toggle = function(force) {
	if(new Date().getTime() < this.lastToggled + 200) {return;}
	this.lastToggled = new Date().getTime();
	if(force === undefined || typeof(force) != "boolean") {
		this.open = !this.open;
	} else {
		this.open = force;
	}
	if(this.open) {
		this.element.className = "sideNav active";
		this.backgroundBlocker.style.display = "block";
		var t = this.backgroundBlocker;
		window.requestAnimationFrame(function() {window.requestAnimationFrame(function() {t.style.opacity = '1.0';});});
	} else {
		this.element.className = "sideNav";
		this.backgroundBlocker.style.opacity = '0.0';
		var t = this.backgroundBlocker;
		window.setTimeout(function() {
			t.style.display = "none";
		},200);
	}
	this.element.removeAttribute('style');
	this.element.removeAttribute('sliding');
}
/**
 * Internal function setting the physical location of the SideNav for CSS to render
 */
SideNav.prototype._updateSNPos = function() {
	this.element.setAttribute('sliding','1');
	if(this.sx === null || this.sx2 === null) {
		this.element.style.webkitTransform = "translateX(-280px)";
		this.element.style.transform = "translateX(-280px)";
		this.backgroundBlocker.style.opacity = '0.0';
		var t = this.backgroundBlocker;
		window.setTimeout(function() {t.style.display = "none";},200);
	} else {
		this.backgroundBlocker.style.display = "block";
		var d = this.sx2-this.sx;
		if(this.open) {
			this.element.style.webkitTransform = "translateX("+d+"px)";
			this.element.style.transform = "translateX("+d+"px)";
			this.backgroundBlocker.style.opacity = 1+d/280;
		} else {
			this.element.style.webkitTransform = "translateX("+(-280 + d)+"px)";
			this.element.style.transform = "translateX("+(-280 + d)+"px)";
			this.backgroundBlocker.style.opacity = d/280;
		}
	}
}

/**
 * Creates an instance of a Snackbar (aka Toast) which displays a given text and buttons for a specific time at the bottom of the screen
 * @param {string} message Message displayed in the snackbar
 * @param {boolean} [autoToggle] if given, if the snackbar should close itself after 30 or `duration` seconds.
 * @param {number} [duration] If `autoToggle` is true, the snackbar will close itself after given seconds
 * @param {Array} [buttons] Array with objects containing `text` and `action` will display buttons at the right side of the snackbar
 */
var Snackbar = function(message,autoToggle,duration,buttons) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Snackbar': Please use the 'new' operator.");}
	if(!window.snackbar_stack) {snackbar_init();}
	this.id = randstr(16);
	this.message = message;
	this.at = autoToggle === true;
	if(this.at && duration != undefined) {
		this.du = parseInt(duration);
	} else {
		this.du = 4;
	}
	this.buttons = [];
	if(buttons != undefined) {
		this.setButtons(buttons);
	}
}
/**
 * Function that sets the buttons for the right side of the snackbar.
 * @param {Array} buttons Array with objects containing `text` and `action`
 */
Snackbar.prototype.setButtons = function(buttons) {
	if(typeof(buttons) != "object") {return;}
	for(var i = 0; i < buttons.length; i++) {
		this.buttons[i] = new Button("raised",buttons[i].text,this.close.bind(this));
		if(buttons[i].action) {
			this.buttons[i].addEvent(buttons[i].action);
		}
	}
}
/**
 * Requests the current snackbar instance to be displayed.  
 * If another snackbar instance is currently visible, this instance will be put in a call stack instead and will be displayed later.
 */
Snackbar.prototype.open = function(force) {
	if(showing_snackbar !== null) {
		if(force) {
			snackbar_stack.unshift(this);
			this.close(true);
			return;
		} else {
			snackbar_stack.push(this);
			return;
		}
	}
	if(last_snackbar_open + 250 > performance.now()) {
		window.setTimeout(this.open.bind(this),Math.max(last_snackbar_open + 250 - performance.now(),0));
		return;
	}
	showing_snackbar = this.id;
	snackbar_content.innerHTML = this.message;
	snackbar_buttons.innerHTML = "";
	for(var i = 0; i < this.buttons.length; i++) {
		snackbar_buttons.appendChild(this.buttons[i].button);
	}
	snackbar_element.setAttribute('visible','1');
	if(this.at) {window.setTimeout(this.close.bind(this),this.du*1000);}
}
/**
 * Requests the current snackbar instance to be closed. The next snackbar from the stack will be displayed after
 */
Snackbar.prototype.close = function(force) {
	if(showing_snackbar === this.id || force) {
		snackbar_element.removeAttribute('visible');
		showing_snackbar = null;
		last_snackbar_open = performance.now();
		if(snackbar_stack.length > 0) {
			var mainstack = snackbar_stack.shift();
			mainstack.open();
		}
	}
}
/**
 * Sets the text of the snackbar. Can only be used **before** the snackbar is visible.
 * @param {string} text 
 */
Snackbar.prototype.setLabel = function(text) {
	this.message = text;
}

/**
 * Creates an instance of Card, which is a 4x3 sized block with an image, title and content.  
 * This card can be clicked upon to transform it to a full sized card, where `content` will be visible too.
 * @param {string} img url of the image
 * @param {string} title text of the title
 * @param {string|Object[]} content string or DOM Node of the content
 * @param {function} errorcallback Function called when som ting wong
 * @param {string} [closedInfo] Text displayed underneath the title when the card is NOT in full screen (optional)
 */
var Card = function(img,title,content,errorcallback,closedInfo) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Card': Please use the 'new' operator.");}
	this.imgurl = img;
	this.title = title;
	this.content = content;
	this.cardOpen = false;
	this.lastCardPosition = null;
	this.locked = false;
	this.touch = false;
	var t = this;
	
	this.card = document.createElement('div',"card");
	this.card.addEventListener('touchstart',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.card.addEventListener('touchmove',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.card.addEventListener('touchend',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.card.addEventListener('scroll',this.handleScroll.bind(this),window.supportsPassive ? {passive:true} : false);
	
	this.image_frame = document.createElement('div',"img_frame");
	this.image = document.createElement('img');
	this.image.style.opacity=0.0;
	this.image.addEventListener('load',function(event) {event.target.style.opacity=1.0;});
	if(errorcallback) {this.image.addEventListener('error',function(event) {errorcallback(event);},window.supportsPassive ? {passive:true} : false);}
	this.image.src = this.imgurl;
	this.image_frame.appendChild(this.image);
	this.card.appendChild(this.image_frame);
	
	this.title_e = document.createElement('h1');
	this.title_e.innerHTML = this.title;
	if(closedInfo) {
		this.closedInfo = document.createElement('div',"closedInfo");
		this.closedInfo.innerHTML = closedInfo;
		this.card.appendChild(this.closedInfo);
		this.title_e.className = "hasCI";
	}
	
	this.headerbar = document.createElement('div',"headerbar");
	this.closeButton = new Button("small","&#xE5CD;",function() {t.openCard(false);});
	this.headerbar.appendChild(this.closeButton.button);
	this.headertitle = this.title_e.cloneNode();
	this.headertitle.innerHTML = this.title;
	this.headertitle.className = "headertitle";
	this.headerbar.appendChild(this.headertitle);
	this.card.appendChild(this.headerbar);
	this.card.appendChild(this.title_e);
	
	this.content_e = document.createElement('div',"card_content");
	this.content_e.innerHTML = this.content;
	this.card.appendChild(this.content_e);
	
	this.element = document.createElement('div',"cardPlaceholder");
	this.element.appendChild(this.card);
	
	this.placeholder = document.createElement('div',"cardSpaceReserver");
}
/**
 * Requests the card to be displayed.
 * @param {boolean} [force] If given, set the card's state instead of toggling it
 * @returns {void|false} returns false if `force` is equal to the card's current state.
 */
Card.prototype.openCard = function(force) {
	if(this.cardOpen === force) {return false;} // It's already open (or closed)!
	if(typeof(force) == "boolean") {
		this.cardOpen = force;
	} else {
		this.cardOpen = !this.cardOpen;
	}
	if(this.cardOpen) {
		this.expandCard();
	} else {
		this.closeCard();
	}
}
/**
 * Internal function to transform the card to fullscreen.
 */
Card.prototype.expandCard = function() {
	if(this.lastCardPosition != null || this.locked) {return;}
	var s = performance.now();
	var pos = this.card.getBoundingClientRect();
	var pos2 = this.element.getBoundingClientRect();
	this.lastCardPosition = pos;
	window.openCard = this;
	this.cardOpen = true;
	this.locked = true;
	this.placeholder.style.width = pos2.width+"px";
	this.placeholder.style.height = pos2.height+"px";
	insertAfter(this.element,this.placeholder);
	this.element.parentNode.style.webkitOverflowScrolling = "initial";
	this.element.parentNode.style.overflowY = "hidden";
	this.element.className = "cardPlaceholder blockSN open";
	this.element.style.transform = "translateX("+(-(window.innerWidth-pos.left-pos.width))+"px) translateY("+(-(window.innerHeight-pos.top-pos.height))+"px)";
	this.card.style.transform = "translateX("+(window.innerWidth-pos.width)+"px) translateY("+(window.innerHeight-pos.height)+"px)";
	this.card.style.transition = "none";
	this.image.style.transition = "none";
	this.element.style.transition = "none";
	this.image.style.transform = "scale("+(pos.width/window.innerWidth)+")";
	var t = this;
	window.requestAnimationFrame(function() {
		window.requestAnimationFrame(function() {
			var s2 = performance.now();
			t.element.style.transform = "";
			t.image.style.transition = "";
			t.element.style.transition = "";
			t.image.style.transform= "";
			t.card.style.transform = "";
			t.card.style.width = "";
			t.card.style.height = "";
			t.card.style.transition = "";
			window.setTimeout(function() {
				/*console.log("Last timeout",performance.now()-e2);*/
				t.locked = false;
				t.closeButton.button.style.opacity=1.0;
			},250);
			var e2 = performance.now();
			/*console.log("First timeout",e2-s2,s2-e1);*/
		});
	});
	var e1 = performance.now();
	/*console.log("Initial execute ",e1-s);*/
}
/**
 * Function to close the card and move it back to its origonal position.
 */
Card.prototype.closeCard = function() {
	if(this.lastCardPosition == null || this.locked) {return false;}
	var s = performance.now();
	this.locked = true;
	window.openCard = null;
	this.cardOpen = false;
	var pos = this.lastCardPosition;
	this.element.style.transform = "translateX("+(-(window.innerWidth-pos.left-pos.width))+"px) translateY("+(-(window.innerHeight-pos.top-pos.height))+"px)";
	this.card.style.transform = "translateX("+(window.innerWidth-pos.width)+"px) translateY("+(window.innerHeight-pos.height)+"px)";
	this.image.style.transform = "scale("+(pos.width/window.innerWidth)+")";
	this.closeButton.button.style.opacity=0.0;
	this.card.scrollTop = 0;
	var t = this;
	window.setTimeout(function() {
		var s2 = performance.now();
		t.placeholder.parentNode.removeChild(t.placeholder);
		t.element.parentNode.style.webkitOverflowScrolling = "";
		t.element.parentNode.style.overflowY = "";
		t.element.className = "cardPlaceholder";
		t.element.style.transform = "";
		t.element.style.transition = "none";
		t.card.style.transform = "";
		t.card.style.transition = "none";
		t.image.style.transition = "none";
		t.image.style.transform = "";
		window.setTimeout(function() {
			/*console.log("Last timeout",performance.now()-e2);*/
			t.element.style.transition = "";
			t.image.style.transition = "";
			t.card.style.transition = "";
			t.lastCardPosition = null;
			t.locked = false;
		},33);
		var e2 = performance.now();
		/*console.log("First timeout",e2-s2,s2-e1);*/
	},250);
	var e1 = performance.now();
	/*console.log("Initial execute ",e1-s);*/
}
/**
 * Internal function handling scrolling (so the title becomes visible if the user scrolls past it)
 * @param {event} event 
 */
Card.prototype.handleScroll = function(event) {
	if(this.card.scrollTop >= window.innerWidth*0.50625-40 && window.innerWidth < 480 || this.card.scrollTop >= window.innerWidth*0.253125-40 && (window.innerWidth < 840 && window.innerWidth >= 480) || this.card.scrollTop >= window.innerWidth*0.16875-40 && window.innerWidth >= 840) {
		if(this.headerbar.className != "headerbar fixed") {
			this.headerbar.className = "headerbar fixed";
		}
	} else {
		if(this.headerbar.className != "headerbar") {
			this.headerbar.className = "headerbar";
		}
	}
}
/**
 * event handler
 * @param {event} event 
 */
Card.prototype.event = function(event) {
	switch(event.type) {
		case 'touchstart':
			if(cordova.platformId == 'android' && system.api_version <= 19) {event.preventDefault();}
			this.touch = true;
		break;
		case 'touchmove':
			this.touch = false;
		break;
		case 'touchend':
			if(this.touch) {
				if(!this.cardOpen) {this.openCard(true);}
				this.touch = false;
			}
		break;
	}
}
/**
 * removes the card from the screen completely
 */
Card.prototype.remove = function() {
	this.element.parentNode.removeChild(this.element);
	this.placeholder.parentNode.removeChild(this.placeholder);
}

/**
 * Creates a new instance of InlineCard. These are slightly different from Card, because they are smaller (screen width, but only 120ish pixels high.)
 * @param {Object} settings
 * @param {string} settings.img url of the image
 * @param {string} settings.title text in the title
 * @param {string} [settings.subTitle] text in the subtitle
 * @param {Object} settings.callbacks object with callback functions
 * @param {function} settings.callbacks.open function called when the card is opened 
 * @param {function} settings.callbacks.remove function called when the card is removed 
 * @param {function} settings.callbacks.leaveBehindPress function called when the card is swept to the right and pressed on the X button. 
 * @param {string} [settings.className] additional class names for CSS
 * @param {boolean} [settings.hasLB] Has Leave-Behind button. This allows the card to be swiped to the left and a "delete" button to be displayed.
 * @param {string} [settings.imageColor] What color the image padding should have. Defaults to #D0D0D0
 */
var InlineCard = function(settings) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'InlineCard': Please use the 'new' operator.");}
	if(!window.inlineCard_element) {inlineCard_init();}
	this.imgurl = settings.img;
	this.title = settings.title;
	this.subtitle = settings.subTitle;
	if(settings.callbacks) {this.callbacks = settings.callbacks;} else {this.callbacks = {};}
	this.className = settings.className; if(!this.className) {this.className = "";}
	this.imageColor = settings.imageColor || "#D0D0D0";
	
	this.hasLB = true;
	if(settings.hasLB != undefined && settings.hasLB != null) {this.hasLB = settings.hasLB;}
	
	this.isTouching = false;
	this.hasMoved = false;
	this.startPos = null;
	
	this.element = document.createElement('div','inlineCard');
	if(this.hasLB) {this.leavebehind = document.createElement('div','leaveBehind',"&#xe14c;",this.element);}
	this.card = document.createElement('div','cardItem '+this.className,null,this.element);
	this.imge = document.createElement('img','card_image',null,this.card);
	this.titlee = document.createElement('span','card_title',this.title,this.card);
	this.subtitlee = document.createElement('span','card_subtitle',this.subtitle,this.card);
	
	this.imge.src = this.imgurl;

	var e = this;
	this.imge.addEventListener('load',function() {e.imge.className = "card_image loaded";},window.supportsPassive ? {passive:true} : false);
	
	this.card.addEventListener('touchstart',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
	this.card.addEventListener('touchend',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
	this.card.addEventListener('touchmove',this.handleTouch.bind(this),window.supportsPassive ? {passive:true} : false);
	if(this.callbacks.leaveBehindPress && this.hasLB) {this.leavebehind.addEventListener('click',this.callbacks.leaveBehindPress,window.supportsPassive ? {passive:true} : false);}
}
/**
 * Returns the DOM Node containing the inline part of the card
 */
InlineCard.prototype.getElement = function() {
	return this.element;
}
/**
 * Internal function to move the card to the left for it's Leave Behind
 * @param {number} delta pixels moved
 */
InlineCard.prototype.moveLB = function(delta) {
	this.cardDelta = delta;
	if(this.lbVisible) {this.cardDelta -= 60;}
	if(this.cardDelta < -60) {this.cardDelta = -60;}
	else if(this.cardDelta > 0) {this.cardDelta = 0;}
	this.card.style.webkitTransform = "translateX("+this.cardDelta+"px)";
	this.card.style.transform = "translateX("+this.cardDelta+"px)";
}
/**
 * Function called when the card is released and it's determened if the leavebehind should be displayed.
 */
InlineCard.prototype.releaseLB = function() {
	this.card.removeAttribute('style');
	this.card.className = this.card.className.replace(" leftBehind","");
	this.lbVisible = false;
	if(this.cardDelta < -30) {
		// Set to open
		this.card.className += " leftBehind";
		this.lbVisible = true;
	}
}
/**
 * Internal function handling touch and cursor events
 * @param {event} event 
 */
InlineCard.prototype.handleTouch = function(event) {
	switch(event.type) {
		case 'click':
			this.open();
		break;
		case 'touchstart':
		//	if(cordova.platformId == 'android' && system.api_version <= 19) {event.preventDefault();}
			this.isTouching = true;
			this.hasMoved = false;
			this.startPos = event.touches;
		break;
		case 'touchend':
			if(this.isTouching) {
				if(!this.hasMoved) {
					// Item was tapped.
					if(this.lbVisible) {
						/*this.card.className = this.card.className.replace(" leftBehind","");
						this.lbVisible = false;*/
					} else {
						this.open();
					}
				} else {
					//this.releaseLB();
				}
				this.isTouching = true;
			}
		break;
		case 'touchmove':
			if(this.isTouching) {
				this.hasMoved = true;
				return;
				if(cordova.platformId == 'android' && system.api_version <= 19) {return;} // LeaveBehind shouldn't work in android 4.4 or lower. (else scrolling doesn't work)
				if(this.hasLB) {
					var deltaX = event.touches[0].clientX - this.startPos[0].clientX;
					var deltaY = event.touches[0].clientY - this.startPos[0].clientY;
				//	console.log(deltaX,deltaY);
					this.moveLB(deltaX);
				}
			}
		break;
		default:
			console.log(event);
	}
}
/**
 * Requests the inline card instance to be displayed
 */
InlineCard.prototype.open = function() {
	if(window.showingInlineCard == null) {
		window.inlineCard_element.className = "cardParent open blockSN";
		window.inlineCard_error.style.display = "none";
		window.inlineCard_imageContainer.style.backgroundColor = this.imageColor;
		window.showingInlineCard = this;
		window.inlineCard_title.innerHTML = this.title;
		window.inlineCard_headerTitle.innerHTML = this.title;
		if(/*cordova.platformId == 'android' && system.api_version <= 19 &&*/ this.hasLB) {
			window.inlineCard_deleteButton.button.style.display='block';
		} else {
			window.inlineCard_deleteButton.button.style.display='none';
		}
		window.inlineCard_loader.show();
		window.inlineCard_loader.start();
		if(this.callbacks.open) {
			this.callbacks.open({target:window.inlineCard_element});
		}
	}
}
/**
 * Requests any inline card to close
 */
InlineCard.prototype.close = function() {
	window.inlineCard_element.className = "cardParent close blockSN";
	window.setTimeout(function(){
		window.inlineCard_element.className = "cardParent blockSN";
		window.showingInlineCard = null;
		window.inlineCard_image.src = "";
		window.inlineCard_image.className = "cardImage";
		window.inlineCard_text.innerHTML = "";
	},250);
}
/**
 * Removes the inline card from the DOM
 */
InlineCard.prototype.remove = function() {
	if(this.element && this.element.parentNode) {
		this.element.style.webkitTransform = "translateX(-100%)";
		this.element.style.transform = "translateX(-100%)";
		var t = this.element;
		window.setTimeout(function() {
			t.parentNode.removeChild(t);
		},500);
		return true;
	}
	return false;
}



/**
 * Creates a new Input instance, which is a fancy shell around a <input> tag.
 * @param {Object} params List with parameters
 * @param {string} [params.label] text displayed in / above the input tag
 * @param {string} [params.helpertext] text displayed underneath the input tag
 * @param {string} [params.errortext] Text displayed when the input value fails to validate
 * @param {string|number} [params.value] Current value in the input tag
 * @param {string} [params.type] type of input tag (e.g. text, number, date, textarea)
 * @param {number} [params.step] step for number
 * @param {number} [params.min] minimal value for number
 * @param {number} [params.max] maximum value for number
 * @param {boolean} [params.noBorder] whether to display a border
 * @param {string} [params.backgroundColor] background color of the input tag
 * @param {string} [params.color] text color of the input tag
 * @param {string} [params.cssText] additional css text for the input tag
 * @param {function} [params.onChange] function called onChange()
 * @param {number} [params.rows] rows for textarea
 * @param {string} [params.name] name of the input tag
 * @param {boolean} [params.requiredLabel] whether to display the label
 * @param {boolean} [params.required] whether the input is required
 * @param {boolean} [params.optional] whether the input is optional (default)
 */
var Input = function(params) {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Input': Please use the 'new' operator.");}
	this.label = "";
	this.helpertext = "";
	this.errortext = "";
	this.value = "";
	this.type = "text";
	this.step = "";
	this.min = null;
	this.max = null;
	this.border = true;
	this.showsLabel = false;
	this.showshelper= false;
	this.showserror = false;
	this.disabled = false;
	this.required = false;
	this.requiredLabel = true;
	this.optional = false;
	this.labelFloating  = false;
	this.counterEnabled = false;
	this.counterType = 0; // 0 = letter, 1 = word
	this.counterLimit = 40;
	this.counter = 0;
	this.backgroundColor = null;
	this.color = null;
	this.rows = 24;
	this.name = "";
	this.events = {change:null,input:null,focus:null,blur:null,keydown:null};
	
	if(params.requiredLabel !== undefined) {this.requiredLabel = params.requiredLabel;}
	if(params.label) {this.label=params.label;if(this.label){this.showsLabel=true;}}
	if(params.helpertext) {this.helpertext=params.helpertext;if(this.helpertext){this.showshelper=true;}}
	if(params.required) {this.required=params.required;if(this.label && this.requiredLabel) {this.label += " *";}}
	if(params.optional) {this.optional=params.optional;if(this.label) {this.label += " (Optioneel)";}}
	if(params.errortext) {this.errortext=params.errortext;} else if(this.label && this.required) {this.errortext = this.label+" is verijst";}
	if(params.value) {this.value = params.value;if(this.value) {this.labelFloating=true;}}
	if(params.type) {this.type = params.type;}
	if(params.min) {this.min = params.min;}
	if(params.max) {this.max = params.max;}
	if(params.noBorder) {this.border = false;}
	if(params.backgroundColor) {this.backgroundColor = params.backgroundColor;}
	if(params.color) {this.color = params.color;}
	if(params.cssText) {this.cssText = params.cssText;}
	if(params.onChange) {this.events.change = params.onChange;}
	if(params.onKeydown) {this.events.keydown = params.onKeydown;}
	if(params.rows) {this.rows = params.rows;}
	if(params.step) {this.step = params.step;}
	if(params.name) {this.name = params.name;}
	
	this.e = {}; // e as in \e\lements
	
	this.e.wrapper = document.createElement('div',"input_wrap");
	if(this.backgroundColor) {
		this.e.wrapper.style.backgroundColor = this.backgroundColor;
	}
	if(this.cssText) {
		this.e.wrapper.style.cssText += this.cssText;
	}
	if(this.type == 'textarea') {
		this.e.input = document.createElement('textarea');
		this.e.input.rows = this.rows;
		this.e.wrapper.setAttribute('textarea','');
	} else if(this.type == 'number') {
		this.e.input = document.createElement('input');
		if(this.step) {
			this.e.input.setAttribute('step','0.1');
		} else if(window.cordova && window.cordova.platformId == "ios") {
			// Fix ios input types failing big time. Only apply this fix if there's no step (whole numbers only)
			this.type = "text";
			this.e.input.setAttribute('pattern','[0-9]*');
		}
	} else {
		this.e.input = document.createElement('input');
	}
	this.e.input.value = this.value;
	this.e.input.name = this.name;
	this.e.input.addEventListener('focus',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.addEventListener('blur',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.addEventListener('input',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.addEventListener('keydown',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.addEventListener('keyup',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.addEventListener('change',this.event.bind(this),window.supportsPassive ? {passive:true} : false);
	this.e.input.setAttribute('type',this.type);
	if(this.required) {this.e.input.setAttribute('required','required');}
	if(this.min !== null) {
		if(this.type == 'number') {
			this.e.input.setAttribute('min',this.min);
		} else {
			this.e.input.setAttribute('minlen',this.min);
		}
	}
	if(this.max !== null) {
		if(this.type == 'number') {
			this.e.input.setAttribute('max',this.max);
		} else {
			this.e.input.setAttribute('maxlen',this.max);
		}
	}
	if(this.color) {
		this.e.input.style.color=this.color;
	}
	this.e.wrapper.appendChild(this.e.input);
	
	this.e.label = document.createElement('span',"label"+(this.labelFloating ? " float" : ""));
	this.e.label.innerHTML = this.label;
	if(this.color) {
		this.e.label.style.color=this.color;
	}
	this.e.wrapper.appendChild(this.e.label);
	
	this.e.border = document.createElement('div',"border");
	if(this.border && this.type !== "textarea") {
		this.e.wrapper.appendChild(this.e.border);
	}
	
	this.e.helpertext = document.createElement('span',"helpertext");
	this.e.helpertext.innerHTML = this.helpertext;
	this.e.wrapper.appendChild(this.e.helpertext);
	if(this.color) {
		this.e.helpertext.style.color=this.color;
	}
	
	this.e.errortext = document.createElement('span',"errortext");
	this.e.wrapper.appendChild(this.e.errortext);
}
Input.prototype.setAttribute = function(name,value) {
	this.e.input.setAttribute(name,value);
	this[name] = value;
}
Input.prototype.getAttribute = function(name) {
	return this.e.input.getAttribute(name) || this[name];
}
/**
 * Function to be called on specific events
 * @param {string} event_name name of the event
 * @param {function} callback function to be called
 */
Input.prototype.addEventListener = function(event_name,callback) {
	this.events[event_name] = callback;
}
/**
 * Sets the current value of the input
 * @param {string} value
 */
Input.prototype.setValue = function(value) {
	this.value = value;
	if(this.value && !this.labelFloating) {
		this._floatLabel();
	} else if(!this.value && this.labelFloating) {
		this._restLabel();
	}
	this.e.input.value = this.value;
}
/**
 * internal function to move the label to the top
 */
Input.prototype._floatLabel = function() {
	this.e.label.className = "label float";
	this.labelFloating = true;
}
/**
 * internal function to reset the label's position
 */
Input.prototype._restLabel = function() {
	this.e.label.className = "label";
	this.labelFloating = false;
}
/**
 * function that returns the input wrapper DOM Node
 */
Input.prototype.getElement = function() {
	return this.e.wrapper;
}
/**
 * Function handling events
 * @param {event} event 
 */
Input.prototype.event = function(event) {
	switch(event.type) {
		case "focus":
			this._floatLabel();
			this.e.wrapper.className = "input_wrap focus";
			if(window.fix_keyboard && window.StatusBar && window.cordova && window.cordova.platformId == "android") {
				document.body.setAttribute('keyboardShown',1);
				window.requestAnimationFrame(function() {
					window.requestAnimationFrame(function() {
						window.requestAnimationFrame(function() {
							StatusBar.show();
							if(window.system && window.system.hasNavBar) {
								document.body.className = document.body.className.replace("NavBarVisible","");
							}
						});
					});
				});
				if(window.statusBarShowerTimeout !== null) {
					window.clearTimeout(window.statusBarShowerTimeout);
				}
			}
			if(this.e.input.scrollIntoViewIfNeeded) {
				var that = this;
				window.setTimeout(function() {that.e.input.scrollIntoViewIfNeeded();},500);
			}
		break;
		case "blur":
			this.e.wrapper.className = "input_wrap";
			this.checkValidity();
			if(window.fix_keyboard && window.StatusBar && window.cordova && window.cordova.platformId == "android") {
				window.statusBarShowerTimeout = window.setTimeout(function() {
					document.body.removeAttribute('keyboardShown');
					window.requestAnimationFrame(function() {
						window.requestAnimationFrame(function() {
							window.requestAnimationFrame(function() {
								window.requestAnimationFrame(function() {
									if(window.system) {
										if(system.api_version > 19) {
											StatusBar.transparent();
										}
										if(system.hasNavBar) {
											document.body.className += " NavBarVisible";
										}
									}
								});
							});
						});
					});
					window.statusBarShowerTimeout = null;
				},50);
			}
		break;
		case 'input':
			this.value = this.e.input.value;
		break;
		default:
		//	console.log(event.type);
	}
	if(this.events[event.type]) {
		this.events[event.type](event);
	}
}
/**
 * Function that sets the disabled state of the input
 * @param {boolean} disable true is disabled, false is enabled
 */
Input.prototype.disable = function(disable) {
	this.disabled = disable;
	if(this.disabled) {
		this.e.wrapper.className = "input_wrap disabled";
		this.e.input.setAttribute('disabled',true);
	} else {
		this.e.wrapper.className = "input_wrap";
		this.e.input.removeAttribute('disabled');
	}
}
/**
 * blurs the input node
 */
Input.prototype.blur = function() {
	this.e.input.blur();
}
/**
 * focusses the input node
 */
Input.prototype.focus = function() {
	this.e.input.focus();
}
/**
 * Checks for if the input value is valid. Shows a animation if it's not the case
 * @param {boolean} [skipAnimation] whether to skip the error animation
 */
Input.prototype.checkValidity = function(skipAnimation) {
	if(!skipAnimation) {
		if(!this.value || (this.e.input.validity && !this.e.input.validity.valid)) {
			if(this.required || (this.e.input.validity && !this.e.input.validity.valid)) {
				this.e.wrapper.className = "input_wrap error";
				//this.errortext = event.target.validationMessage;
				if(window.lang) {
					this.errortext = lang.translate("invalid_value");
				} else {
					this.errortext = "Ongeldig";
				}
				this.e.errortext.innerHTML = this.errortext;
			} else {
				this._restLabel();
			}
		}
	}
	return this.e.input.checkValidity();
}

/**
 * Returns an instance of Stepper, which is basically a form with steps and drop-down lists
 * 
 * **This feature is highly experimental**
 */
var Stepper = function() {
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Stepper': Please use the 'new' operator.");}
	this.steps = [];
	this.element = document.createElement('div',"stepper");
	
}
/**
 * returns the stepper wrapper DOM Node
 */
Stepper.prototype.getElement = function() {
	return this.element;
}
/**
 * Opens a specific step while closing the other ones.
 * @param {HTMLElement} stepper DOM Node of the step to be displayed
 */
Stepper.prototype.showSpecificStepper = function(stepper) {
	for(var i = 0; i < this.steps.length; i++) {
		if(i !== stepper) {
			this.steps[i].setVisible(this,false);
		} else {
			this.steps[i].setVisible(this,true);
		}
	}
}
/**
 * Creates a new step
 * @param {Object} properties 
 * @param {string} properties.name 
 * @param {boolean} properties.showsNextButton 
 * @param {string} properties.nextButtonText 
 * @param {function} properties.nextButtonAction 
 * @param {boolean} properties.showsPreviousButton 
 */
Stepper.prototype.createItem = function(properties) {
	var i = this.steps.length;
	this.steps[i] = {
		properties:properties,
		visible: false,
		element: document.createElement('div'),
		contentBlock: document.createElement('div'),
		content: document.createElement('div'),
		setVisible: function(stepper,visible) {
			stepper.steps[i].visible = visible;
			if(visible) {
				stepper.steps[i].contentBlock.className = "stepper_content visible";
				stepper.steps[i].element.className = "stepper_item activated";
			} else {
				stepper.steps[i].contentBlock.className = "stepper_content";
			}
		}
	};
	this.steps[i].element.className = "stepper_item";
	this.steps[i].element.innerHTML = "<span class='step_count'>"+(i+1)+"</span>"+properties.name;
	var t = this;
	this.steps[i].element.addEventListener('click',function(){
		t.showSpecificStepper(i);
	},window.supportsPassive ? {passive:true} : false);
	this.element.appendChild(this.steps[i].element);
	this.steps[i].contentBlock.className = "stepper_content";
	this.steps[i].contentBlock.appendChild(this.steps[i].content);
	this.element.appendChild(this.steps[i].contentBlock);
	if(properties.showsNextButton) {
		var label = properties.nextButtonText || "Volgende";
		if(properties.nextButtonAction) {
			this.steps[i].nextButton = new Button("colored",label,properties.nextButtonAction,null,null,"margin:4px 4px 4px 0px;");
		} else {
			this.steps[i].nextButton = new Button("colored",label,function(e) {
				// Next step
				if(t.steps[i+1]) {
					t.steps[i].setVisible(t,false);
					t.steps[i+1].setVisible(t,true);
					t.steps[i+1].element.scrollIntoView();
				}
			},null,null,"margin:4px 4px 4px 0px;");
		}
		this.steps[i].contentBlock.appendChild(this.steps[i].nextButton.button);
	}
	if(properties.showsPreviousButton) {
		this.steps[i].prevButton = new Button("transparent","Vorige",function(e) {
			// Next step
			if(t.steps[i-1]) {
				t.steps[i].setVisible(t,false);
				t.steps[i-1].setVisible(t,true);
				t.steps[i-1].element.scrollIntoView();
			}
		},null,null,"margin:4px 0px 4px 4px;");
		this.steps[i].contentBlock.appendChild(this.steps[i].prevButton.button);
	}
	if(i == 0) {
		this.steps[i].visible = true;
		this.steps[i].element.className = "stepper_item activated";
		this.steps[i].contentBlock.className = "stepper_content visible";
	}
	
	return this.steps[i].content;
}

/**
 * Creates a new instance of a Selector, which is basically a <select>
 * @param {Object} properties 
 * @param {Array} [properties.options] Array with objects with name and value
 * @param {boolean} [properties.hideBaseElement] Unimplemented feature
 */
var Selector = function(properties) {
	if(!properties) {var properties = {};}
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'Selector': Please use the 'new' operator.");}
	this.element = document.createElement('div','selector');
	this.element_wrap = document.createElement('div','selectorWrap',null,this.element);
	if(properties.options) {
		this.options = properties.options;
	} else {
		this.options = [];
	}
	this.elements = [];
	this.selectedIndex = 0;
	this.value = "";
	this.disabled = false;
	this.baseElement = document.createElement('div','selectorBase',null,this.element);
	if(properties.hideBaseElement) {
		this.baseElement.style.display="none";
	}

	this.isOpen = false;
	this.opened = 0;
	this.callbacks = {};
	this._draw();
	this.moved = false;
	this.baseMoved = false;
	if(window.is_pc) {
		document.body.addEventListener('mousedown',this._handleBodyClick.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		document.body.addEventListener('touchstart',this._handleBodyClick.bind(this),window.supportsPassive ? {passive:true} : false);
	}
	this.bindEvents();
}
Selector.prototype.bindEvents = function() {
	var t = this;
	if(window.is_pc) {
		this.baseElement.addEventListener('click',function() {window.setTimeout(t.open.bind(t));},window.supportsPassive ? {passive:true} : false);
	} else {
		this.baseElement.addEventListener('touchstart',function() {t.baseMoved = false;},window.supportsPassive ? {passive:true} : false);
		this.baseElement.addEventListener('touchmove',function() {t.baseMoved = true;},window.supportsPassive ? {passive:true} : false);
		this.baseElement.addEventListener('touchend',function() {if(!t.baseMoved) {window.setTimeout(t.open.bind(t))};},window.supportsPassive ? {passive:true} : false);
	}
}
Selector.prototype.setAttribute = function(name,value) {
	this.element.setAttribute(name,value);
	this[name] = value;
}
Selector.prototype.getAttribute = function(name) {
	return this.element.getAttribute(name) || this[name];
}
/**
 * Returns the DOM Node of the wrapper
 */
Selector.prototype.getElement = function() {
	return this.element;
}
/**
 * Returns the DOM Node of the visible selector node (at which the user can click on)
 */
Selector.prototype.getBaseElement = function() {
	return this.element;
}
/**
 * Toggles disabled / enabled
 * @param {boolean} state If the selector should be clickable
 */
Selector.prototype.setEnabled = function(state) {
	this.disabled = !state;
	this._draw();
}
/**
 * Adds a function to be called for a specific event
 * @param {string} name Name of the event
 * @param {function} callback Function to be called
 */
Selector.prototype.addEventListener = function(name,callback) {
	this.callbacks[name] = callback;
}
/**
 * Sets the options, same as on the construct function
 * @param {Array} options Array of objects with name and value
 */
Selector.prototype.setOptions = function(options) {
	this.options = options;
	this._draw();
}
/**
 * Selects a index, same as Select.selectedIndex
 * @param {number} i Index of the item to select
 */
Selector.prototype.selectIndex = function(i) {
	if(typeof(i) == "number") {
		this.selectedIndex = i;
	} else if(!isNaN(parseInt(i))) {
		this.selectedIndex = parseInt(i);
	}
	for(var i = 0; i < this.elements.length; i++) {
		this.elements[i].className = "selectorOption";
		if(i == this.selectedIndex) {
			this.elements[i].className += " selected";
			this.baseElement.innerHTML = this.options[i].name;
			this.value = this.options[i].value;
		}
	}
}
/**
 * shows the selector (not entirely implemented yet)
 */
Selector.prototype.open = function() {
	this.element_wrap.className = "selectorWrap visible";
	this.isOpen = true;
	this.opened = performance.now();
	// Temporary add an event listener for a click event, so we can close the dropdown thingie.
}
/**
 * Closes the selector (not entirely implemented yet)
 */
Selector.prototype.close = function() {
	this.element_wrap.className = "selectorWrap closing";
	this.isOpen = false;
	var t = this;
	window.setTimeout(function() {
		if(!t.isOpen && document.body.className.indexOf('compat') === -1) {
			t.element_wrap.className = "selectorWrap";
		}
	},150);
	//document.body.removeEventListener('touchstart',this._handleBodyClick.bind(this));
}
/**
 * Re-draws the HTML DOM Nodes for the selector
 */
Selector.prototype._draw = function() {
	this.elements = [];
	this.element_wrap.innerHTML = "";
	this.selectedIndex = 0;
	
	for(var i = 0; i < this.options.length; i++) {
		this.elements[i] = document.createElement('span','selectorOption',this.options[i].name,this.element_wrap);
		this.elements[i].setAttribute('value',this.options[i].value);
		if(window.is_pc) {
			this.elements[i].addEventListener('click',this._handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
		} else {
			this.elements[i].addEventListener('touchstart',this._handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
			this.elements[i].addEventListener('touchend',this._handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
			this.elements[i].addEventListener('touchmove',this._handleClick.bind(this),window.supportsPassive ? {passive:true} : false);
		}
	
		if(this.selectedIndex == i) {
			this.baseElement.innerHTML = this.options[i].name;
			this.value = this.elements[0].getAttribute('value') || this.elements[0].innerHTML;
		}
	}
	if(this.disabled) {
		this.baseElement.className = "selectorBase disabled";
		if(this.isOpen) {this.close();}
	} else { // enabled
		this.baseElement.className = "selectorBase";
	}
}
/**
 * Internal function to handle touch and click events on all other elements
 * @param {event} event 
 */
Selector.prototype._handleBodyClick = function(event) {
	if(!this.isOpen) {return;}
	if(this.disabled) {return;}
	if(performance.now() - this.opened < 100) {
		// Clicked within .1 a second of opening, ignore event.
		return;
	}
	var clickedOnMe = false;
	if(event.path && event.path.indexOf) {
		if(event.path.indexOf(this.element) >= 0) {clickedOnMe = true;}
	} else {
		var target = event.target;
		while(target !== document.body) {
			if(target == this.element) {clickedOnMe = true;}
			target = target.parentNode;
		}
	}
	if(!clickedOnMe) {
		this.close();
	}
}
/**
 * Internal function to handle Click and touch events
 * @param {event} event 
 */
Selector.prototype._handleClick = function(event) {
	switch(event.type) {
		case 'touchstart':
			if(window.cordova && window.system && cordova.platformId == 'android' && system.api_version <= 19) {event.preventDefault();}
			this.moved = false;
		break;
		case 'touchmove':
			this.moved = true;
		break;
		case 'click':
			this.moved = false;
		case 'touchend':
			if(!this.moved) {
				var oldValue = this.value;
				var oldIndex = this.selectedIndex;
				this.selectedIndex = this._getPropertyIndex(event.target);
				this.value = this.options[this.selectedIndex].value || event.target.getAttribute('value') || event.target.innerHTML;
				this.baseElement.innerHTML = this.options[this.selectedIndex].name;
				if(this.callbacks.change && (oldValue !== this.value || oldIndex !== this.selectedIndex)) {
					this.callbacks.change({type:'change',detail:"change",target:this.element});
				}
				this.close();
				for(var i = 0; i < this.elements.length; i++) {
					this.elements[i].className = "selectorOption";
					if(i == this.selectedIndex) {
						this.elements[i].className += " selected";
					}
				}
			}
		break;
	}
}/**
 * Get the index of a list item
 * @param {HTMLElement} el DOM Node of the item
 * @returns {number|null} Number or null
 */
Selector.prototype._getPropertyIndex = function(el) {
	for(var i = 0; i < this.elements.length; i++) {
		if(this.elements[i] == el) {return i;}
	}
	return null;
}

/**
 * Creates an instance of a MutliSelector, which is basically a <select multiple>
 * @param {Object} properties 
 * @param {number} [properties.maxValues] 
 * @param {Array} [options] 
 */
var MultiSelector = function(properties,options) {
	// Properties: object with things like names, max amount of items selected, colores etc. Options: array with objects with names and titles, and if they're checked or not.
//	if(new.target === undefined) {throw("TypeError: Failed to construct 'MultiSelector': Please use the 'new' operator.");}
	if(!properties || typeof(properties) != "object") {throw("ArgumentError: Properties is an required argument.");}
	this.element = document.createElement('div',"multipleSelector");
	this.options = options;
	if(this.options) {
		this.buildDom();
	}
	this.elements = [];
	this.maxValues = null;
	if(properties.maxValues !== undefined) {this.maxValues = properties.maxValues;}
}
/**
 * Returns the wrapper DOM Node
 */
MultiSelector.prototype.getElement = function() {
	return this.element;
}
/**
 * Sets the Options, same as in the construct function
 * @param {Array} options Array with Objects with name and value keys.
 */
MultiSelector.prototype.setOptions = function(options) {
	if(!options || typeof(options) != "object") {throw("ArgumentError: Options is an required argument.");}
	this.options = options;
	this.buildDom();
}
/**
 * Builds the DOM Nodes
 */
MultiSelector.prototype.buildDom = function() {
	if(!this.options || typeof(this.options) != "object") {return false;}
	this.elements = [];
	this.element.innerHTML = "";
	for(var i = 0; i < this.options.length; i++) {
		this.elements[i] = this.createOption(i,this.options[i]);
		this.element.appendChild(this.elements[i].container);
	}
}
/**
 * Creates a new option DOM Node
 * @param {number} i Index of the option. Not actually used yet
 * @param {Object} data Information about the option
 * @param {string} data.title Title to be displayed as the option
 * @param {string} data.name Name of the checkbox
 * @param {string} data.value Value of the option
 */
MultiSelector.prototype.createOption = function(i,data) {
	var e = {value:data.value,container:document.createElement('div'),title:document.createElement('span')};
	e.container.className = "multipleSelector_option";
	e.title.className = "multipleSelector_optiontitle";
	e.title.innerHTML = data.title;
	e.container.appendChild(e.title);
	e.checkbox = new Checkbox(data.value,data.name);
	var t = this;
	e.checkbox.addEventListener('change',function(event) {
		if(e.checkbox.value && (t.maxValues == null || t.getValues().length < t.maxValues)) {
			e.value = e.checkbox.value;
		} else if(e.checkbox.value) {
			return false; // Cancel the change.
		} else if(!e.checkbox.value) {
			e.value = e.checkbox.value;
		}
	},window.supportsPassive ? {passive:true} : false);
	e.container.appendChild(e.checkbox.getElement());
	e.container.addEventListener('click',function(event) {
		if(event.path.indexOf(e.checkbox.getElement()) == -1) {
			e.checkbox.handleClick(null);
		}
	},window.supportsPassive ? {passive:true} : false);
	return e;
}
/**
 * Returns the currently selected values
 */
MultiSelector.prototype.getValues = function() {
	var r = [];
	for(var i = 0; i < this.elements.length; i++) {
		if(this.elements[i].value) {
			r[r.length] = this.options[i].name;
		}
	}
	return r;
}

/**
 * Creates an instance of Tabs
 */
var Tabs = function() {
	this.sX = null;
	this.sY = null;
	this.sM = false;
	this.lM = false;
	this.sD = 0;
	this.sDy = 0;
	this.sP = 0;
	this.sMaxrows = 0;
	
	
	this.tabs = [];
	this.element = document.createElement('div','tabcontainer');
	this.header = document.createElement('ul','tabheader');
	
	
	if(window.system && window.system.api_version <= 19) {return;} // We shouldn't allow swiping for android 4.4 or lower because of a huuuuuge issue with touchmove in that version.
	this.element.addEventListener('touchstart',this._handleSliding.bind(this),window.supportsPassive ? {passive:true} : false);
	this.element.addEventListener('touchmove',this._handleSliding.bind(this),window.supportsPassive ? {passive:true} : false);
	this.element.addEventListener('touchend',this._handleSliding.bind(this),window.supportsPassive ? {passive:true} : false);
//	window.addEventListener('orientationchange',this._handleOrientation.bind(this),supportsPassive ? {passive:true} : false);
}
/**
 * Returns the Tabs DOM Node
 */
Tabs.prototype.getElement = function() {
	return this.element;
}
/**
 * Returns the Tabs Header DOM Node
 */
Tabs.prototype.getHeader = function() {
	return this.header;
}
/**
 * Resets the entire structure
 */
Tabs.prototype.clearAll = function() {
	this.tabs = [];
	this.element.innerHTML = "";
	this.header.innerHTML = "";
	this.sMaxrows = 0;
	this.sP = 0;
	this.element.style.webkitTransform = "";
	this.element.style.transform = "";
}
/**
 * Creates a new tab
 * @param {Object} data Information about the tab
 * @param {string} data.title Title of the tab
 * @param {HTMLElement} data.content Content inside the tab
 */
Tabs.prototype.addTab = function(data) {
	if(!data.content || !data.title) {return false;}
	var index = this.tabs.length;
	
	this.tabs[index] = document.createElement('div','tab');
	this.tabs[index].appendChild(data.content);
	
	var headItem = document.createElement('li',null,data.title,this.header);
	headItem.setAttribute('data-index',index);
	headItem.addEventListener('click',this._clickListItem.bind(this),window.supportsPassive ? {passive:true} : false);
	if(index == 0) {headItem.className = 'active';}
	this.element.appendChild(this.tabs[index]);
	this.sMaxrows = index;
	return index;
}
/**
 * Interal function for clicking on a list
 * @param {event} event 
 */
Tabs.prototype._clickListItem = function(event) {
	var index = event.target.getAttribute('data-index');
	if(!index) {return;}
	this._switchTab(parseInt(index));
}
/**
 * Internal function switching tabs to a specific index
 * @param {number} index 
 */
Tabs.prototype._switchTab = function(index) {
	this.headItems = this.header.childNodes;
	if(this.headItems[this.sP]) {this.headItems[this.sP].className = "";}
	if(this.headItems[index]) {this.headItems[index].className = "active";this._scrollIntoView(this.headItems[index]);}
	this.sP = index;
	this.element.style.webkitTransform = "translateX(-"+(100*this.sP)+"%)";
	this.element.style.transform = "translateX(-"+(100*this.sP)+"%)";
}
/**
 * Internal function that moves the header horizontal slider on-screen for the tab that was selected
 * @param {HTMLElement} item HTML Element of the item
 */
Tabs.prototype._scrollIntoView = function(item) {
	var runs = 0;
	if(item.offsetLeft - this.header.scrollLeft < 0) {
		while(item.offsetLeft - this.header.scrollLeft < 0 && this.header.scrollLeft != 0 && runs < 1000) {
			this.header.scrollLeft--;
			runs++;
		}
	} else {
		while(item.offsetLeft - this.header.scrollLeft + item.offsetWidth > window.innerWidth && runs < 1000) {
			this.header.scrollLeft++;
			runs++;
		}
	}
}
/**
 * Internal function handling touch events
 * @param {event} event 
 */
Tabs.prototype._handleSliding = function(event) {
	switch(event.type) {
		case 'touchstart':
			//if(cordova.platformId == 'android') {event.preventDefault();}
			this.sX = event.touches[0].clientX;
			this.sY = event.touches[0].clientY;
			this.element.setAttribute('sliding','1');
		break;
		case 'touchend':
			if(!this.lM) {
				if(this.sD > window.innerWidth/3 && this.sP < this.sMaxrows) {
					// Moved 1/3rd to the right
					this._switchTab(this.sP+1);
				} else if(this.sD < -window.innerWidth/3 && this.sP !== 0) {
					// Moved 1/3rd to the left
					this._switchTab(this.sP-1);
				} else {
					this._switchTab(this.sP);
				}
			} else {
				this._switchTab(this.sP);
			}
			this.element.removeAttribute('sliding');
			this.sX = null;
			this.sY = null;
			this.sM = false;
			this.lM = false;
			this.sD = 0;
			this.sDy = 0;
		break;
		case 'touchmove':
			if(this.sX) {
				this.sD = this.sX-event.touches[0].clientX;
				this.sDy = this.sY-event.touches[0].clientY;
				if(this.sM && !this.lM) {
					this.element.style.webkitTransform = "translateX(-"+(window.innerWidth*this.sP+this.sD)+"px)";
					this.element.style.transform = "translateX(-"+(window.innerWidth*this.sP+this.sD)+"px)";
				} else if(this.lM) {
					// Movement Locked (LockedMovement)
				} else if(!this.lM) {
					if(Math.abs(this.sD) > 10) {
						this.sM = true;
					} else if(Math.abs(this.sDy) > 10) {
						this.lM = true;
					}
				}
			}
	}
}

var RefreshIcon = function(parentNode,scrollNode,defaultHeight) {
	this.isVisible = false;
	this.isDragging = false;
	this.dragHeight = 0;
	this.defaultHeight = defaultHeight || 0;
	this.maxDragHeight = 100;
	this.dragYpos = null;
	this.events = {
		reload: null
	};
	this.element = document.createElement('div','refreshIcon');
	this.loader = new loader();
	this.loader.setSize(24*2);
	this.loader.setColor('#03A9F4');
	this.element.appendChild(this.loader.getElement());
	this.textElement = document.createElement('span',null,'&#xE5D5;',this.element);
	
	
	//if(window.cordova && cordova.platformId == "ios") {return;} // Going to skip ios for this, since its overflow scrolling is glithy enough as it is!


	this.parentNode = parentNode;
	this.scrollNode = scrollNode;
	if(this.parentNode) {
		this._bindEvents();
		this.parentNode.appendChild(this.element);
	}

}
RefreshIcon.prototype.addEventListener = function(name,handler) {
	this.events[name] = handler;
	return true;
}
RefreshIcon.prototype._bindEvents = function() {
	if(window.system && system.api_version <= 19) {return;}
	this.parentNode.addEventListener('touchstart',this._handleEvent.bind(this),supportsPassive ? {passive:true} : false);
	this.parentNode.addEventListener('touchend',this._handleEvent.bind(this),supportsPassive ? {passive:true} : false);
	this.parentNode.addEventListener('touchmove',this._handleEvent.bind(this),supportsPassive ? {passive:true} : false);
}
RefreshIcon.prototype._handleEvent = function(event) {
	switch(event.type) {
		case 'touchstart':
			if(!this.isDragging && this.scrollNode.scrollTop <= 0) { // also include less than, because iPhones can overscroll.
				this.isDragging = true;
				this.dragHeight = 0;
				this.dragYpos = event.clientY || event.touches[0].clientY;
				this.isVisible = true;
			}
		break;
		case 'touchend':
			if(this.isDragging) {
				if(this.dragHeight == this.maxDragHeight) {
					this.startRefresh();
				} else {
					this.hide();
				}
				this.isDragging = false;
			}
			this.dragHeight = 0;
			this.dragYpos = null;
		break;
		case 'touchmove':
			if(this.isDragging) {
				var posY = event.clientY || event.touches[0].clientY;
				var deltaY = (posY - this.dragYpos) / 2; /// divide by 2, gives the user more room
				deltaY = Math.max(Math.min(deltaY,this.maxDragHeight),-32);
				var proc = deltaY / this.maxDragHeight;
				this.textElement.style.webkitTransform = "rotate("+(proc*360)+"deg)";
				this.textElement.style.transform = "rotate("+(proc*360)+"deg)";
				this.dragHeight = deltaY;
				this.element.style.webkitTransform = "translateY("+(deltaY+this.defaultHeight-48)+"px)";
				this.element.style.transform = "translateY("+(deltaY+this.defaultHeight-48)+"px)";
			}
		break;
		default: 
			console.warn("Unknown event: ",event.type);
	}
}
RefreshIcon.prototype.startRefresh = function() {
	console.log('Starting refresh');
	this.loader.show();
	this.loader.start();
	if(this.events.reload) {
		this.events.reload();
	}
}
RefreshIcon.prototype.hide = function() {
	if(!this.isVisible) {return;}
	this.element.style.webkitTransitionDelay = "150ms";
	this.element.style.transitionDelay = "150ms";
	this.element.style.webkitTransform = "translateY(0px)";
	this.element.style.transform = "translateY(0px)";
	var t = this;
	window.setTimeout(function() {
		t.element.style.webkitTransitionDelay="";
		t.element.style.transitionDelay="";
		t.textElement.style.webkitTransform = "rotate(0deg)";
		t.textElement.style.transform = "rotate(0deg)";
		t.loader.stop();
		t.loader.hide();
		t.isVisible = false;
	},150);
}