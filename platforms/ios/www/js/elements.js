"use strict";

/**
 * **elements**  
 * A library of all DOM elements.  
 * Items can be added, changed, removed etc.
*/
var elements = {
	sideNav: {},
	messages: {},
	domHasBeenBuilt: false,
	/**
	 * Function called to create the webpage in which the app resides.
	 */
	buildDom: function() {
		if(this.domHasBeenBuilt) {return;}
		this.app = document.getElementsByClassName('app')[0];
		
		this.loginLoader = new loader();
		this.loginLoader.setSize(160);
		this.app.appendChild(this.loginLoader.getElement());
		
		this.createPage('loginPage',false);
		this.loginPage.className += " blockSN";
		this.loginBlurBG = document.createElement('div','blurbg');
		this.loginPage.insertBefore(this.loginBlurBG,this.loginPageContent);

		this.loginLogo = document.createElement("img","logo_img",null,this.loginPageContent);
		this.loginLogo.src = "img/staples-logo.png";

		this.loginForm = document.createElement('div','loginForm',null,this.loginPageContent);

		this.loginMessageDisplay = document.createElement('div','loginMsgDisp','Deze toepassing is ontworpen voor een bepaalde groep van onze klanten.<br>Neem voor meer informatie contact op met uw persoonlijke accountmanager.',this.loginForm);

		this.apiKeyInputWrap = document.createElement('div','inputWrap',null,this.loginForm);
		this.inputAPIKey = new Input({
			label: "Staples-Scanner Inlogcode",
			type: "password",
			onKeydown: function(event) {if(event.keyCode == 13) {elements.loginSubmitBtn.clickAction[0]();}}
		});
		this.apiKeyInputWrap.appendChild(this.inputAPIKey.getElement());

		document.createElement('div','login_icon material-icons','&#xe897;',this.apiKeyInputWrap);

		this.loginSubmitBtn = new Button("colored","Aanmelden",function(event) {
			var value = elements.inputAPIKey.value;
			if(!value || !value.trim()) {
				return new Dialog('alert','Geen inlogcode opgegeven!','Waarschuwing').open();
			}
			app.login(elements.inputAPIKey.value);
		});
		this.loginForm.appendChild(this.loginSubmitBtn.button);

		// #################
		
		this.createPage('homePage',false);

		this.sideNav = new SideNav();
		this.app.appendChild(this.sideNav.element);
		this.app.appendChild(this.sideNav.backgroundBlocker);

		this.sideNav.setTitle("Staples Scanner App<br><small>V"+version+(showBuildCode ? "<div class='buildCode'>"+buildCode+"</div>" : "")+"</small>");

		this.sideNav.addItem("Niet-verzonden orders",'&#xe8b0;',function() {
			app.showCabinetBacklog();
		},null,true);
		this.sideNav.addItem('Laatst Bijgewerkt','&#xe2c4;',function() {
			app.showDownloadStatus();
		},null,false);
		this.sideNav.addItem("Uitloggen",'&#xe9ba;',function() {
			app.logout();
			elements.sideNav.toggle();
		},null,false);
		
		this.scannerLogo = document.createElement("img","logo_img",null,this.homePageContent);
		this.scannerLogo.src = "img/staples-logo.svg";
		
		this.homePageImgBlock = document.createElement('div','imgblock',null,this.homePageContent);

		this.spanKeyStr = document.createElement('span','keySpan',null,this.homePageImgBlock);

		this.spanVersion = document.createElement('span','versionCode',"<small>Versie</small><br>"+version,this.homePageImgBlock);


		//this.dataAgeDisplay = document.createElement('div',null,"Laatst bijgewerkt: ",this.homePageContent);
		//this.dataAgeSpan = document.createElement('span',null,null,this.dataAgeDisplay);
		//document.createElement('span',null,' geleden.',this.dataAgeDisplay);
		//this.forceReloadBtn = new Button('small','&#xe627;',function(event) {
		//	app.loadCabinets();
		//	app.loadArticles();
		//	app.loadNews();
		//});
		//this.forceReloadBtn.button.style.padding = "0px";
		//this.forceReloadBtn.button.style.margin = "0px";
		//this.forceReloadBtn.contentSpan.className += " material-icons";
		//this.dataAgeDisplay.appendChild(this.forceReloadBtn.button);
		
		this.headerDisplay = document.createElement('div','headerDisplay',null,this.homePageContent);
		this.newsButton = new Button('small','Meer...',function() {
			app.openNews();
		},'newsButton');
		this.homePageContent.appendChild(this.newsButton.button);
		this.newsDisplay = document.createElement('div','newsDisplay',null,this.homePageContent);
		
		this.cabinetWrapper = document.createElement('table',null,null,this.homePageContent);
		
		this.sideNavButton = new Button("small","&#xe5d2;",function(event) {
			elements.sideNav.toggle();
		},'sideNavButton');
		this.sideNavButton.contentSpan.className += " material-icons";
		this.homePageContent.appendChild(this.sideNavButton.button);
		
		/*this.cabinetLogBtn = new Button("small","&#xe8b0;",function(event) {
			app.showCabinetBacklog();
		},'cabinetLogBtn');
		this.cabinetLogBtn.contentSpan.className += " material-icons";
		this.homePageContent.appendChild(this.cabinetLogBtn.button);*/

		this.manualCabinetBtn = new Button("","Handmatig",function(event) {
			app.inputCabinetManual();
		},'cabinetInputBtn');
		this.homePageContent.appendChild(this.manualCabinetBtn.button);

		this.scanCabinetBtn = new Button("colored","Scan barcode van de kast",function(event) {
			app.scanCabinet();
		},'cabinetScanBtn');
		this.homePageContent.appendChild(this.scanCabinetBtn.button);

		
		// ##############
		
		//this.createPage('cabinetPage',false);
		this.cabinetPage = document.createElement('div','cabinetPage',null,this.app);
		this.cabinetPage.className += " blockSN";
		this.cabinetPageContent = document.createElement('div','content',null,this.cabinetPage);
		this.cabinetPageContentWrap = document.createElement('div','contentWrap',null,this.cabinetPageContent);
		this.cabinetPageContentButtons = document.createElement('div','buttons',null,this.cabinetPageContent);
		
		//this.cabinetLogo = document.createElement("img","logo_img",null,this.cabinetPage);
		//this.cabinetLogo.src = "img/staples-logo.png";
		
		this.cabinetNameplate = document.createElement('div','cabinetName',null,this.cabinetPageContentWrap);
		this.cabinetDetailSect = document.createElement('div','cabinetDetails',null,this.cabinetPageContentWrap);
		
		this.visible_articles = [];
		this.scannedArticlesContainer = document.createElement('div','articleWrapper',null,this.cabinetPageContentWrap);
		
		this.backToScannerBtn = new Button("colored","Klaar",function(event) {
			app.finishCabinet();
		},'backToScannerBtn');
		//this.backToScannerBtn.contentSpan.className += " material-icons";
		this.cabinetPageContentButtons.appendChild(this.backToScannerBtn.button);

		this.scanArticleBtn = new Button("colored","Scan Artikel",function(event) {
			app.scanArticle();
		},'articleScanBtn');
		this.cabinetPageContentButtons.appendChild(this.scanArticleBtn.button);
		
		this.manualArticleBtn = new Button("","Handmatig",function(event) {
			app.inputArticleManual();
		},'articleInputBtn');
		this.cabinetPageContentButtons.appendChild(this.manualArticleBtn.button);
		
		
		// #####################


		var iPhoneDialog = new Dialog('alert','Are you simulating an iPhone X, or Xs, Xsmax, Xr etc?<br><br><small>This message is only displayed in the simulator</small>','Simulator Debugging',[{text:'no'},{text:'yes',action:function() {
			window.device.model = "iPhone10,6";
			system.fixIphoneSupport();
		}}]);
		if(window.device && cordova.platformId == 'ios' && device.isVirtual) {
			iPhoneDialog.open();
		}
		
		this.domHasBeenBuilt = true;
		return true;
	},
	/**
	 * @param {string} name Name of the page used in the variable name
	 * @param {boolean} [hasAppBar] If the page has an AppBar
	 * @param {string} [appBarTitle] title displayed in the AppBar
	 * @param {function} [returnAction] Function called when the user clicks on 'return' or 'close'. Defaults to the SideNav opening
	 */
	createPage: function(name,hasAppBar,appBarTitle,returnAction) {
		this[name] = document.createElement('div',"normalpage "+name,null,this.app);
		if(hasAppBar) {
			this[name+"AppBar"] = document.createElement('div',"appBar",null,this[name]);
			this[name+"AppBarTitle"] = document.createElement('h1',null,(appBarTitle == undefined ? "" : appBarTitle),this[name+"AppBar"]);
			this[name+"AppBarSubTitle"] = document.createElement('span','subTitle',null,this[name+"AppBar"]);
			if(returnAction !== undefined) {
				this[name+"ReturnButton"] = new Button('small','&#xe5c4;',returnAction);
				this[name+"ReturnButton"].button.className += " return_button";
				this[name+"ReturnButton"].off = -30;
				this[name+"AppBar"].appendChild(this[name+"ReturnButton"].button);
			} else if(elements.sideNav) {
				this[name+"SideNavButton"] = new Button('small','&#xe5d2;',function() {app.sideNav.toggle(true);});
				this[name+"SideNavButton"].button.className += " return_button";
				this[name+"SideNavButton"].off = -30;
				this[name+"AppBar"].appendChild(this[name+"SideNavButton"].button);
			}
		}
		this[name+"Content"] = document.createElement('div',"content",null,this[name]);
	},
	/**
	 * Creates a new item in the Settings
	 * @param {HTMLTableElement} table Table in which the row resides
	 * @param {string} name Variable name
	 * @param {string} icon Icon displayed left of text
	 * @param {string} [text] Text displayed in the row
	 * @param {function} [click_action] Function called when the row is tapped 
	 */
	createSettingItem: function(table,name,icon,text,click_action) {
		if(!table || !icon || !name) {return false;}
		var row = document.createElement('tr',null,null,table);
		if(click_action) {
			var isClicking = false;
			row.addEventListener('touchstart',function(e) {isClicking=true;},window.supportsPassive ? {passive:true} : false);
			row.addEventListener('touchmove',function(e) {isClicking=false;},window.supportsPassive ? {passive:true} : false);
			row.addEventListener('touchend',function(e) {if(isClicking) {click_action(e);isClicking=false;}},window.supportsPassive ? {passive:true} : false);
		}
		var icontd = document.createElement('td',null,null,row);
		var texttd = document.createElement('td',null,null,row);
		texttd.setAttribute('colspan',2);
		this["settingitem_"+name+"_icon"] = document.createElement('div',"material-icons",icon,icontd);
		this["settingitem_"+name+"_title"] = document.createElement('span',null,text,texttd);
		document.createElement('br',null,null,texttd);
		this["settingitem_"+name+"_value"] = document.createElement('small',null,null,texttd);
	},
	/**
	 * Creates a new item in the Settings  
	 * Also places an HTML DOM Node at the right side
	 * @param {HTMLTableElement} table Table in which the row resides
	 * @param {string} name Variable name
	 * @param {string} icon Icon displayed left of text
	 * @param {string} [text] Text displayed in the row
	 * @param {HTMLElement} [rightItemElement] HTML DOM Node placed on the right side
	 */
	createSettingItemWithAction: function(table,name,icon,text,rightItemElement) {
		if(!table || !icon || !name) {return false;}
		var row = document.createElement('tr','withRightSetting',null,table);
		var icontd = document.createElement('td',null,null,row);
		var texttd = document.createElement('td',null,null,row);
		var actiontd = document.createElement('td',null,null,row);
		this["settingitem_"+name+"_icon"] = document.createElement('div',"material-icons",icon,icontd);
		this["settingitem_"+name+"_title"] = document.createElement('span',null,text,texttd);
		document.createElement('br',null,null,texttd);
		this["settingitem_"+name+"_value"] = document.createElement('small',null,null,texttd);
		actiontd.appendChild(rightItemElement);
	},
	/**
	 * @param {number} i index number for the app.modules array
	 */
	createModuleCard: function(i) {
		var n = app.modules[i].name_short;
		var ns = "modulecard_"+ns;
		var name = app.modules[i].name;
//		console.log(name,n.toLowerCase(),lang.hasTranslation(n.toLowerCase()));
		this[ns] = new InlineCard({img:"img/modules/"+app.modules[i].name_short+"/banner.jpg",title:name,callbacks:{open:function(event) {
			window.inlineCard_loader.stop();
			window.inlineCard_loader.hide();
			var img = window.inlineCard_image;
			var cont = window.inlineCard_text;
			img.src = "img/modules/"+app.modules[i].name_short+"/banner.jpg";
			cont.innerHTML = app.modules[i].expanded_content;
		}},hasLB:false,imageColor:"white"});
		if(!app.modules[i].active) {this[ns].card.className += " inactive";}
		this.modulesPageContent.appendChild(this[ns].getElement());
	}
};
/**
 * Creates a new instance of AppBarOptionButton
 * 
 * Intended to be displayed at the top right of an appBar, it has an menu list that pops down.
 */
var AppBarOptionButton = function() {
	this.button = new Button('small','&#xe5d4;',this.toggleMenu.bind(this));
	this.button.button.className += " menu_button"
	this.button.off = -30;
	this.menuElement = document.createElement('div','menu_list');
	this.opened = 0;
	this.open = false;
}
/**
 * Returns the Button element
 * @returns {HTMLElement}
 */
AppBarOptionButton.prototype.getButton = function() {
	return this.button.button;
}
/**
 * Returns the Menu list element
 * @returns {HTMLDivElement}
 */
AppBarOptionButton.prototype.getMenu = function() {
	return this.menuElement;
}
/**
 * Creates a new instance of an menuItem and stores it in its array.
 * @param {string} name Display name
 * @param {function} callback Function called when pressed
 * @param {boolean} [shouldKeepList] If set to true, the list will not be closed after the callback function was called
 * @param {boolean} [isDisabled] if the option is enabled or disabled
 */
AppBarOptionButton.prototype.addMenuItem = function(name,callback,shouldKeepList,isDisabled) {
	var menuItem = function(content,callback,shouldKeepList,isDisabled,parent) {
		this.enabled = !isDisabled;
		this.content = content;
		var clickCallback = callback;
		var clc = false;
		this.element = document.createElement('span','menu_item',this.content);
		this.element.addEventListener('touchstart',function() {clc = true;},window.supportsPassive ? {passive:true} : false);
		this.element.addEventListener('touchend',function(event) {
			if(clc) {
				clc = false;
				clickCallback(event);
				if(!shouldKeepList) {
					window.setTimeout(function() {parent.toggleMenu();},250); // slight delay to avoid iOS for thinking we're clicking behind the menu
				}
			}
		},window.supportsPassive ? {passive:true} : false);
		this.element.onclick = function() {}; // attempt to fix iphones, again.
	}
	menuItem.prototype.getElement = function() {
		return this.element;
	}
	menuItem.prototype.toggle = function() {
		this.enabled = !this.enabled;
	}
	menuItem.prototype.enable = function() {
		this.enabled = true;
	}
	menuItem.prototype.disable = function() {
		this.enabled = false;
	}
	var i = new menuItem(name,callback,shouldKeepList,isDisabled,this);
	this.menuElement.appendChild(i.getElement());
	return i;
}
/**
 * Handles events for the dropdown button
 * @param {event} event 
 */
AppBarOptionButton.prototype.handleEvent = function(event) {
	if(performance.now() - this.opened < 100 || !this.open) {
		// Clicked within .1 a second of opening, ignore event.
		return;
	}
	var clickedOnMe = false;
	if(event.path && event.path.indexOf) {
		if(event.path.indexOf(this.menuElement) >= 0) {clickedOnMe = true;}
	} else {
		var target = event.target;
		while(target !== document.body) {
			if(target == this.menuElement) {clickedOnMe = true;}
			target = target.parentNode;
		}
	}
	if(!clickedOnMe) {
		this.open = true;
		this.toggleMenu();
	}
}
/**
 * toggles the menu list.
 * @param {event} event not used
 */
AppBarOptionButton.prototype.toggleMenu = function(event) {
	this.open = !this.open;
	if(this.open) {
		this.opened = performance.now();
		this.menuElement.className = "menu_list visible";
		document.body.addEventListener('touchstart',this.handleEvent.bind(this),window.supportsPassive ? {passive:true} : false);
	} else {
		this.menuElement.className = "menu_list";
		document.body.removeEventListener('touchstart',this.handleEvent.bind(this));
	}
}