"use strict";
/* 
 * General Settings and functions
 * Things like version info and time / date of last update included as well.
*/
var version = "0.0.17";
var date = "05-05-2019";
var creator = "Thijmen de Frankrijker";
var buildCode = version.split(".").slice(1).join('')+"-PRE-2";
var showBuildCode = true;
var appName = "Staples Scanner-App";


var supportsPassive = false;
try {
  var opts = Object.defineProperty({}, 'passive', {
    get: function() {
      supportsPassive = true;
    }
  });
  window.addEventListener("test", null, opts);
} catch (e) {}

/**
 * Searches for an element with the ID `search`, starting at `elm` and working its way to the body.
 * @param {HTMLElement} elm 
 * @param {string} search 
 * @returns {boolean} if the element is found.
 */
function findSource(elm,search) {
	while(elm.id !== search && elm !== document.body) {elm = elm.parentNode;}
	if(elm !== document.body) {return true;}
	return false;
}

/**
 * A function that prints out a given object into a string with `\r\n`
 * 
 * Cuts off at depth `depth`
 * @param {Object[]} obj 
 * @param {number} [depth] Where to cut off the print. Optional. Defaults to 1. Maxes out at 3. 
 */
function ObjToStr(obj,depth) {
	if(!depth) {depth = 1;}
	if(depth > 3) {return obj;}
	try {
		var ret = "";
		for(var x in obj) {
			ret += x+": ";
			if(typeof(obj[x]) == "object") {ret += "\r\n"+ObjToStr(obj[x],depth+1);}
			else {ret += obj[x];}
			ret += "\r\n";
		}
		return ret;
	} catch(ex) {
		return ex;
	}
}

/**
 * Returns the amount of keys in a given object or array.
 * @param {Object[]} object 
 * @return {number}
 */
function count(object) {
	if(typeof(object) == 'object' && object !== null) {
		if(object.length !== undefined && typeof(object.length) == 'number') {return object.length;}
		var c = 0;
		for(var x in object) {c++;}
		return c;
	}
	return 0;
}

function xhr(address,postfields,callback,timeout) {
	var x = new XMLHttpRequest();
	x.open("POST",address,true);
	x.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	x.responseType = "json";
	if(!timeout) {var timeout = 30000;}
	x.timeout = timeout;
	x.onload = function(event) {
		var status = x.status;
		var resp = x.response;
		if(typeof(resp) == "string") {try {resp = JSON.parse(x.responseText);} catch(ex) {} }
		if(status < 200 || status >= 300) {
			callback(status,event);
		} else {
			callback("ok",resp);
		}
	}
	x.onerror = function(event) {
		callback("error",event);
	}
	x.ontimeout = function(event) {
		callback("timeout",event);
	}
	var postStr = "";
	if(typeof(postfields) == "string") {postStr = postfields;}
	else if(typeof(postfields) == "object") {
		for(var i in postfields) {
			postStr += (postStr==""?"":"&")+i+"="+postfields[i];
		}
	}
	x.send(postStr);
}

/**
 * Parses the string with unicode to normal text
 * @param {string} input 
 * @returns {string}
 */
function unicodeToStr(input) {
	var p = input.split("");
	var l = p.length;
	var r = "";
	for(var x = 0; x < l; x++) {
		if(p[x] == "&" && p[x+1] == "#") {
			var c = 2; var n = "";
			while(!isNaN(parseInt(p[x+c])) && c < 11) {n += p[x+c];c++;}
		//	console.log("app.unicodeToStr",n,c);
			r += String.fromCharCode(n);
			x += c;
		} else {
			r += p[x];
		}
		
	}
	return r;
}
/**
 * Prepends "0" to `inp` if its length equals to 1. (e.g. numbers 0-9, but not 10 or more)
 * @param {string|number} inp 
 * @param {number} [len] Optional, defaults to 2
 * @returns {string|number}
 */
function prefix(inp,len) {
	var inp = inp;
	var len = len || 2;
	if(isNaN(parseFloat(inp))) {return inp;}
	while(inp.toString().length < len) {inp = "0"+inp;}
	return inp;
}
/**
 * Returns a random value from an object or array.
 * @param {Object[]|Array} object 
 * @returns {*}
 */
function getRandomIndex(object) {
	var c = count(object);
	var t = Math.floor(Math.random()*c);
	var n = 0;
	for(var i in object) {
		if(n == t) {return object[i];}
		n++;
	}
	return null;
}
/**
 * Returns the keys in a array or object. Similar to PHP's array_keys function.
 * @param {Array|Object[]} array 
 * @returns {Array} Array with keys
 */
function array_keys(array) {
	var keys = [];
	for(var i in array) {keys.push(i);}
	return keys;
}
/**
 * Uses a regex to parse href URL's to use window.open instead (this fixes a crashing bug)
 * @param {string} txt 
 * @returns {string}
 */
function parseLinks(txt) {
	// Prase links = replace <a href='http(s)://xxxxxx'> with <a href="javascript:open('http(s)://xxxxxxx')">
	//var regex = new RegExp('href=\"(http(?:s|)://[\w\.\\\/\&\#\+\-\;]+)\"',"g");
	var regex = new RegExp('href="(http(?:s|)://[\\w\.\/\&\\+\\;\\-\\,]+)"',"g");
	var ret = txt.replace(regex,"href onClick=\"window.open('$1','_system');return false;\""); // href=\"javascript:open('$1')\"
	return ret;
}

/**
 * Gets the OS version of the device based upon the user agent. (unrelyable)
 * @param {string} useragent 
 * @param {string} platform 
 */
function getOsVersion(useragent,platform) {
	if(platform == "android") {
		var u = useragent.substring(useragent.indexOf("Linux;")+7);
		u = u.substring(8,u.indexOf(";"));
		return u;
	} else if(platform == "ios") {
		var u = useragent.substring(useragent.indexOf("OS ")+3);
		u = u.substring(0,u.indexOf("like Mac"));
		return u.replace(/_/g,".");
	}
	return null;
}

/** class TimeObject   
 * uses the Date object's toLocale to correctly format date strings
 * @param {number|Date} time Time in UTC *or* a time object
 */
var TimeObject = function(time) {
	this.time = null;
	if(typeof(time) == "object") {
		this.time = time;
	} else if(typeof(time) == "number") {
		this.time = new Date(time);
	} else {
		this.time = new Date();
	}
	this.defaultLocale = "en-GB";
}
/**
 * Sets the time
 * @param {number|Date} time Time in UTC *or* a time object
 * @returns {Date}
 */
TimeObject.prototype.setDate = function(time) {
	if(typeof(time) == "object") {
		this.time = time;
	} else if(typeof(time) == "number") {
		this.time = new Date(time);
	}
	return this.time;
}
/**
 * Gives the time for the locale for the time object (e.g. en-US, nl-NL)
 * @param {string} [locale] 
 * @returns {string}
 */
TimeObject.prototype.toString = function(locale) {
	if(!locale) {var locale = this.defaultLocale;}
	return this.time.toLocaleString(locale);
}
/**
 * Gives the time for a specific locale, with custom options
 * 
 * **Unfinished, only year and seconds are supported by 'none'**
 * @param {Object[]} options 
 * @param {string} options.year 
 * @param {string} options.seconds 
 * @param {*} [locale] 
 * @returns {string}
 */
TimeObject.prototype.toCustomString = function(options,locale) {
	if(!locale) {var locale = this.defaultLocale;}
	var d = this.time.toLocaleString(locale);
	
	if(options) {
		if(options.year) {
			if(options.year == "none") {
				d = d.replace(/[-/\.]?\d\d\d\d(?:[-/\.]|(,? ))/,"$1");
			}
		}
		if(options.seconds) {
			if(options.seconds == "none") {
				d = d.replace(/(\d:\d\d):\d\d/,"$1");
			}
		}
	}
	return d;
}
/**
 * Returns toLocaleDateString for a (optional) given locale
 * @param {string} [locale] 
 * @returns {string}
 */
TimeObject.prototype.getDate = function(locale) {
	if(!locale) {var locale = this.defaultLocale;}
	return this.time.toLocaleDateString(locale);
}
/**
 * Returns toLocaleTimeString for a (optional) given locale
 * @param {string} [locale] 
 * @returns {string}
 */
TimeObject.prototype.getTime = function(locale) {
	if(!locale) {var locale = this.defaultLocale;}
	return this.time.toLocaleTimeString(locale);
}

if(typeof(CustomEvent) == undefined) {
	CustomEvent = function(type,events) {
		return {type:type,detail:(events ? (events.detail ? events.detail : null) : null)};
	}
}

function copyObject(src) {
	var target = {};
	for (var prop in src) {
	  if (src.hasOwnProperty(prop)) {
		target[prop] = src[prop];
	  }
	}
	return target;
  }

