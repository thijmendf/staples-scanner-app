"use strict";
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    statusBarColor: "FFFFFF",
    addresses: {
        test: {
            apiKey: "https://scanners.staplesweb.nl/apikey.php",
            cabinets: "https://scanners.staplesweb.nl/cabinet.php",
            articles: "https://scanners.staplesweb.nl/product.php",
            submitOrder: "https://scanners.staplesweb.nl/order.php",
            news: "https://scanners.staplesweb.nl/news.php",
            image: "https://scanners.staplesweb.nl/image.php?productcode="
        },
        live: { 
            apiKey: "https://scanner.staplesweb.nl/apikey.php",
            cabinets: "https://scanner.staplesweb.nl/cabinet.php",
            articles: "https://scanner.staplesweb.nl/product.php",
            submitOrder: "https://scanner.staplesweb.nl/order.php",
            news: "https://scanner.staplesweb.nl/news.php",
            image: "https://scanner.staplesweb.nl/image.php?productcode="
        }
    },
    appMode: 'live',
    /*scannerSettings: {
        types: {
            Code128: false,
            Code39: true,
            Code93: false,
            CodaBar: false,
            DataMatrix: false,
            EAN13: false,
            EAN8: false,
            ITF: false,
            QRCode: false,
            UPCA: false,
            UPCE: false,
            PDF417: false,
            Aztec: false
        },
        detectorSize: {
            width: 0.3,
            height: 0.2
        }
    },*/
    scannerSettings: {
        preferFrontCamera : false, // iOS and Android
        showFlipCameraButton : true, // iOS and Android
        showTorchButton : true, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        saveHistory: true, // Android, save scan history (default false)
        prompt : "Place a barcode inside the scan area", // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        formats : "CODE_39", // default: all but PDF_417 and RSS_EXPANDED
        //orientation : "", // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations : false, // iOS
        disableSuccessBeep: true // iOS and Android
    },
    cabinets: [],
    articles: [],
    newsData: {},
    cabinetBacklog: [],
    currentCabinetList: {},
    username: null,
    data_age: 0,
    viewingPage: null,
    scanned_article: null,
    scanned_article_id: null,
    scanned_cabinet: null,
    scanned_cabinet_id: null,
    isScanning: false,
    forceLoadList: false,
    downloadStatus: {
        cabinets: {
            status: 0,
            items: 0,
            age: 0
        },
        articles: {
            status: 0,
            items: 0,
            age: 0
        }
    },
    orderCodes: [
        "API Key ingetrokken",
        "API Key is niet geldig",
        "Geen kasten gevonden voor deze API Key",
        "Geen kasten gekoppeld voor deze API Key",
        "Geen kast(en) gevonden",
        "Geen producten gevonden",
        "API Key niet ingegeven",
        "Order is reeds eerder verzonden",
        "Order kan niet worden aangemaakt",
        "Order is goed verzonden",
        ""
    ],
    showsFileAge: false,
    showSpecificPage: function(page_e) {
		if(this.lockPageSwitch > 0 && this.lockPageSwitch < 2) {this.lockPageSwitch++;return;}
		this.lockPageSwitch = 1;
		if(this.viewingPage) {
			if(this.viewingPage == page_e) {return;}
			this.viewingPage.removeAttribute('viewing');
			this.viewingPage.setAttribute('hiding','');
		}
		page_e.style.display='block';
		window.setTimeout(function() {
			page_e.setAttribute('viewing','');
		},33);
		window.setTimeout(function() {
			app.lockPageSwitch = 0;
			if(app.viewingPage) {app.viewingPage.removeAttribute('hiding');app.viewingPage.style.display='';}
			app.viewingPage = page_e;
		},350);
	},
	hideAllPages: function() {
		var e = document.getElementsByClassName('normalpage');
		for(var i = 0; i < e.length; i++) {
			e[i].style.display="none";
		}
	},
    startup: function() {
        localforage.getItem('cabinetBacklog',function(err,data) {
            if(err) {return;} // it's fine to not have data, it just means we have no backlog.
            if(data === null) {return;} // we don't want nulls either.
            // there's data, put it in memory and attempt to submit it as well.
            app.cabinetBacklog = data;
        });
        window.setInterval(app.updateAgeDisplay.bind(app),1*1000);
        localforage.getItem('username',function(err,result) {
            if(err || !result) {return;}
            app.username = result;
        });
        localforage.getItem('data_age',function(err,result) {
            if(result) {app.data_age = parseInt(result);}
            app.updateAgeDisplay();
        });
        localforage.getItem('testMode',function(err,data) {
            if(data) {app.appMode = 'test';}
        });
        localforage.getItem('open_article',function(err,data) {
            if(data) {
                app.scanned_cabinet = data.scanned_cabinet;
                app.scanned_cabinet_id = data.scanned_cabinet_id;
                app.currentCabinetList = data.currentCabinetList;
                app.forceLoadList = true;
            }
        });
        localforage.getItem('apiKey',function(err,result) {
            if(err) {
                console.log(err);
                // show login page
                app.showSpecificPage(elements.loginPage);
            } else {
                console.log("apiKey from local storage: ",result);
                if(!result) { // Api key is false, null, undefined, 0 etc
                    return app.showSpecificPage(elements.loginPage);
                }
                elements.loginLoader.show();
                elements.loginLoader.start();
                app.pollApiKey(result,function(data) {
                    elements.loginLoader.stop();
                    elements.loginLoader.hide();
                    if(data && data.status) { // the key is valid
                        app.loginFunctions(result,data);
                    } else { // the key is invalid.
                        app.showSpecificPage(elements.loginPage);
                        localforage.removeItem('apiKey');
                        localforage.removeItem('cabinetBacklog');
                        app.cabinetBacklog = [];
                        elements.sideNav.setActiveList(0,false);
                    }
                },function(name,error) { // there was an error, just assume the key is still valid
                    elements.loginLoader.stop();
                    elements.loginLoader.hide();
                    console.log('HTTP error - ',name);
                    new Dialog("alert","U kunt de kast wel scannen, de order zal worden opgeslagen op dit apparaat en worden verzonden wanneer er weer verbinding met het internet is.","Geen Verbinding").open();
                    app.loginFunctions(result,{
                        name: app.username,
                        test: app.appMode == 'test'
                    });
                })
            }
        });
    },
    login: function(key) {
        var d = new Dialog('loader','Laden...');
        d.open();
        this.pollApiKey(key,function(data) {
            console.log(data);
            d.close();
            // success
            if(!data.status) {
                var reason = "";
                switch(data.reasoncode) {
                    case 1:
                        // invalid api key
                        reason = "Ongeldige Inlogcode";
                    break;
                }
                var a = new Dialog('alert',reason || data.reason,'Waarschuwing');
                a.open();
            } else {
                app.loginFunctions(key,data);
            }
        }, function(type,status) {
            console.log(status);
            d.close();
            var str = type;
            switch(status.target.status) {
                case 0:
                    str = "Geen internetverbinding (0)";
                break;
                case 403:
                    str = "Server heeft de toegang geweigerd (403)";
                break;
                case 404:
                    str = "Server kent opgevraagde bestand niet (404)";
                break;
                case 500:
                    str = "Foutcode op server (500)";
                break;
                case 502:
                    str = "Foutcode op server (502)";
                break;
                default:
                    console.warn(type);
            }
            // failed
            var a = new Dialog('alert',str,'Fout');
            a.open();
        })
    },
    loginFunctions: function(key,data) {
        localforage.setItem('apiKey',key); 
        app.apiKey = key;
        if(data) {
            if(data.test)  {
                app.appMode = 'test';
                localforage.setItem('testMode',true);
            } else {
                localforage.setItem('testMode',false);
            }
            if(data.name) {
                app.username = data.name;
                localforage.setItem('username',app.username);
                elements.spanKeyStr.innerHTML = "<small>Ingelogd als</small><br>"+app.username;
            }
        }
        app.loadCabinets();
        app.loadArticles();
        app.loadNews();
        app.submitOrderBacklog();
        if(!app.forceLoadList) {
            app.showSpecificPage(elements.homePage);
        } else {
            elements.loginLoader.show();
            elements.loginLoader.start();
        }
    },
    logout: function() {
        var a;
        if(app.cabinetBacklog.length !== 0) {
            a = new Dialog('alert','Er staan nog onverstuurde orders in de app. Als u uitlogt, gaan deze verloren. Toch uitloggen?','Uitloggen',[
                {text:"Terug"},
                {text:"Uitloggen",action:app._logout.bind(app),filled:true}
            ]);
        } else {
            a  = new Dialog('alert','Weet u zeker dat u wilt uitloggen?',"Uitloggen",[
                {text:"Terug"},
                {text:"Uitloggen",action:app._logout.bind(app),filled:true}
            ]);
        }
        a.open();
    },
    _logout: function() {
        localforage.removeItem('apiKey');
        this.apiKey = null;
        localforage.removeItem('cabinetBacklog');
        this.cabinetBacklog = [];
        elements.spanKeyStr.innerHTML =  "";
        this.showSpecificPage(elements.loginPage);
        elements.sideNav.setActiveList(0,false);
    },
    updateAgeDisplay: function() {
        if(!this.showsFileAge) {return;}
        var str = "";
        var keynames = {cabinets:"Kasten",articles:"Artikelen"};
        var statusnames = ["Onbekend","Gedownload","Fout bij downloaden","Geladen vanuit cache"];
        for(var n in this.downloadStatus) {
            str += "<h2>"+keynames[n]+"</h2>";
            str += "<p>Status: "+statusnames[this.downloadStatus[n].status]+"<br>Aantal items: "+this.downloadStatus[n].items+"<br>Bijgewerkt: "+this.parseTime(this.downloadStatus[n].age)+" geleden<br>";
        }
        document.getElementById('fileAgeDisplay').innerHTML = str;
    },
    parseTime: function(ms) {
        var n = [["seconde","minuut","uur","dag"],["seconden","minuten","uur","dagen"]];
        var da = new Date().getTime();
        var diff = Math.floor((da - ms)/1000);
        var s = diff%60;
        var m = Math.floor(diff/60)%60;
        var h = Math.floor(diff/60/60)%24;
        var d = Math.floor(diff/60/60/24);
        if(m == 0 && h == 0 && d == 0) {
            if(s == 0) {
                return "enkele ogenblikken";
            } else {
                return s + " " + n[s==1?0:1][0];
            }
        } else if(h == 0 && d == 0) {
            return m + " " + n[m==1?0:1][1];
        } else if(d == 0) {
            return h + " " + n[h==1?0:1][2];
        } else {
            var str = d + " " + n[d==1?0:1][3];
            if(h > 0) {
                str += " "+h+" "+n[h==1?0:1][2];
            }
            return str;
        }
    },
    showDownloadStatus: function() {
        var a = new Dialog('alert',"<div id='fileAgeDisplay'>...</div>",'Laatst Bijgewerkt',[
            {text:"Nu vernieuwen",action:function() {
                app.loadCabinets();
                app.loadArticles();
                app.loadNews();
            },keepOpen:true},
            {text:"Sluit",action:function() {
                app.showsFileAge = false;
            }}
        ]);

        if(a.open()) {
            app.showsFileAge = true;
        }

    },
    pollApiKey: function(key,onSuccess,onFail) {
        xhr(this.addresses.live.apiKey,{apiKey:key},function(status,data) {
            if(status == "ok" && data && data.status) {
                onSuccess(data);
            } else {
                xhr(app.addresses.test.apiKey,{apiKey:key},function(status,data) {
                    if(status == "ok") {
                        data.test = true;
                        onSuccess(data);
                    } else {
                        onFail(status,data);
                    }
                });
            }
        },10000);
    },
    loadCabinets: function() {
        localforage.getItem('cabinets',function(err,data) {
            if(data) {app.cabinets = data;}
            app.downloadStatus.cabinets.status = 3;
            app.downloadStatus.cabinets.items = count(data);
            app.updateAgeDisplay();
            var start = performance.now();
            xhr(app.addresses[app.appMode].cabinets,{apiKey:app.apiKey},function(status,data) {
                var end = performance.now();
                console.log("Loaded cabinets -", status, 'in',end-start,'ms');
                app.downloadStatus.cabinets.status = 2;
                if(status == "ok") {
                    if(data.status) {
                        app.cabinets = data.data;
                        localforage.setItem('cabinets',app.cabinets);
                        app.data_age = new Date().getTime();
                        localforage.setItem('data_age',app.data_age);
                        app.downloadStatus.cabinets = {status:1,age:app.data_age,items:count(app.cabinets)};
                        app.updateAgeDisplay();
                    }
                }
            },10000);
        });
    },
    loadArticles: function() {
      localforage.getItem('articles',function(err,data) {
            if(data) {app.articles = data;}
            app.downloadStatus.articles.status = 3;
            app.downloadStatus.articles.items = count(data);
            app.updateAgeDisplay();
            var start = performance.now();
            xhr(app.addresses[app.appMode].articles,{apiKey:app.apiKey},function(status,data) {
                var end = performance.now();
                console.log("Loaded Articles -", status, 'in',end-start,'ms');
                app.downloadStatus.articles.status = 2;
                if(status == "ok") {
                    if(data.status) {
                        app.articles = data.data;
                        localforage.setItem('articles',app.articles);
                        app.data_age = new Date().getTime();
                        localforage.setItem('data_age',app.data_age);
                        app.downloadStatus.articles = {status:1,age:app.data_age,items:count(app.articles)};
                        app.updateAgeDisplay();
                    }
                }
                if(app.forceLoadList) {
                    elements.loginLoader.stop();
                    elements.loginLoader.hide();
                    app.displayCabinetPage();
                }
            },10000);
      });  
    },
    loadNews: function() {
        xhr(app.addresses[app.appMode].news,{apiKey:app.apiKey,appversion:version},function(status,data) {
            if(status == "ok") {
                if(data.status) {
                    app.newsData = data;
                    app.printNews();
                }
            }
        });
    },
    printNews: function() {
        if(!this.newsData.status) {return;}
        if(this.newsData.header) {
            var text = parseLinks(this.newsData.header);
            elements.headerDisplay.innerHTML = text;
        }
        if(this.newsData.data && this.newsData.data.length) {
            elements.newsButton.button.style.display="block";
            /*
            var rkey = getRandomIndex(this.newsData.data);
            var text = parseLinks(rkey);
            elements.newsDisplay.innerHTML = text;
            */
        } else {
            elements.newsButton.button.style.display="none";
        }
    },
    openNews: function() {
        if(!app.newsData || !app.newsData.data || !app.newsData.data.length) {return;}
        var str = "";
        for(var i = 0; i < app.newsData.data.length; i++) {
            str += app.newsData.data[i]+"<hr>";
        }
        new Dialog('alert',parseLinks(str),null).open();
    },
    searchCabinets: function(cabinet) {
        if(!(cabinet in this.cabinets)) {return null;}
        return this.cabinets[cabinet];
    },
    scanCabinet: function() {
        app.startScan("Scan de identificatie-barcode van de kast",app.handleCabinetScan);
    },
    handleCabinetScan: function(data) {
        console.log(data);
        if(data.cancelled) {return;}
        /*if(error) {
            var a = new Dialog('alert',error,'Fout');
            return a.open();
        }*/
        var row = app.searchCabinets(data.text);
        if(!row) {
            var a = new Dialog("alert","Kast niet gevonden.","Fout");
            return a.open();
        }
        app.scanned_cabinet = row;
        app.scanned_cabinet_id = data.text;
        app.displayCabinetPage();
    },
    inputCabinetManual: function() {
        var d = new Dialog(
            'prompt',
            {
                code:{label:"Kast code",type:"number",onKeydown:function(event) {
                    console.log(event);
                    if(event.keyCode == 13) {
                        d.buttons[1].action({code:event.target.value});
                        d.close();
                    }
                }}
            },
            "Handmatig kast-code invoeren",
            [
                {text:"Annuleren"},
                {text:"Ingeven",colored:true,action: function(data) {
                    var code = data.code;
                    if(!code) {return;}
                    app.handleCabinetScan({
                        cancelled:false,
                        text:code
                    })
                }}
            ]
        );
        d.open();
    },
    searchArticles: function(article) {
        if(!(article in this.articles)) {return null;}
        return this.articles[article];
    },
    scanArticle: function() {
        app.startScan("Scan Artikel",app.handleArticleScan);
    },
    handleArticleScan: function(data) {
        console.log(data);
        if(data.cancelled) {return;}
        var isManual = false;
        if(data.manual) {
            isManual = true;
        }
        var row = app.searchArticles(data.text);
        if(!row) {
            var a = new Dialog("alert","Artikel niet gevonden.","Fout",[
                {text:"Sluit"},
                (!isManual ? {text:"Probeer Opnieuw",action:app.scanArticle,filled:true} : null)
            ]);
            return a.open();
        }
        // now check if this article can be scanned for this cabinet
        var ok = false;
        if(!app.scanned_cabinet.products) {
            ok = true;
        } else {
            for(var i in app.scanned_cabinet.products) {
                if(i == data.text) {ok = true;}
            }
        }
        if(!ok) {
            var a = new Dialog("alert","Artikel niet in assortiment van deze kast.","Fout",[
                {text:"Sluit"},
                {text:"Probeer Opnieuw",action:app.scanArticle,filled:true}
            ]);
            a.open();
            return;
        }
        // check item status
        if(row.status === 1) {
            var d = new Dialog('alert',"Verwijder de locatiekaart, want het product is niet meer leverbaar en zal worden vervangen","Niet leverbaar");
            d.open();
            return;
        }

        // check if it's already been scanned. Display the a modified version of the 'add' page instead.
        app.scanned_article = row;
        app.scanned_article_id = data.text;
        if(data.text in app.currentCabinetList) {
            app.displayArticleCounterEdited();
        } else {
            app.displayArticleCounter(isManual);
        }
    },
    inputArticleManual: function() {
        var d = new Dialog(
            'prompt',
            {
                code:{label:"Artikel code",type:"number",onKeydown:function(event) {
                    if(event.keyCode == 13) {
                        d.buttons[1].action({code:event.target.value});
                        d.close();
                    }
                }}
            },
            "Handmatig artikel-code invoeren",
            [
                {text:"Annuleren"},
                {text:"Opgeven",colored:true,action: function(data) {
                    var code = data.code;
                    if(!code) {return;}
                    app.handleArticleScan({
                        cancelled:false,
                        text:code,
                        manual: true
                    })
                }}
            ]
        );
        d.open();
    },
    displayCabinetPage: function() {
        elements.cabinetNameplate.innerHTML = this.scanned_cabinet.name;
        elements.cabinetDetailSect.innerHTML = "<br>";
        if(this.scanned_cabinet.contact_name && this.scanned_cabinet.contact_phone) {elements.cabinetDetailSect.innerHTML += ""+this.scanned_cabinet.contact_name+" ("+this.scanned_cabinet.contact_phone+")<br>";}
        if(this.scanned_cabinet.contact_message) {elements.cabinetDetailSect.innerHTML += this.scanned_cabinet.contact_message+"<br>";}
        if(!this.forceLoadList) {this.currentCabinetList = {};}
        this.updateArticleList();
        elements.cabinetPage.style.display = "block";
        elements.cabinetPageContent.className = "content";
        this.forceLoadList = false;
    },
    displayArticleCounter: function(isManual) {
        var min = null, max = null;
        if(this.scanned_cabinet.products) {
            if(this.scanned_cabinet.products[this.scanned_article_id]) {
                min = this.scanned_cabinet.products[this.scanned_article_id].min;
                max = this.scanned_cabinet.products[this.scanned_article_id].max;
            }
        }
        console.log("Give amount for article",app.scanned_article);
        var a = new Dialog('prompt',{
            /*labelName: ,*/
            inputAmount: {type:"number",type:"number",label:"Aantal",value:'1',min:1,name:"largeNumberInput"},
            labelAmount: (min || max ? "Minimaal: "+min+", Maximaal: "+max : "")
        },app.scanned_article.name,[
            {text:"Klaar",filled:true,action:function(event) {
                app.addArticleToList(event.inputAmount);
            }},
            (!isManual ? {text:"Scan volgende",filled:true,action:function(event) {
                app.addArticleToList(event.inputAmount);
                app.scanArticle();
            }} : null),
            {text:"Annuleren"}
        ]);
        a.open();
    },
    displayArticleCounterEdited: function() {

        console.log("Give amount for article",app.scanned_article);
        var prevAmount = app.currentCabinetList[app.scanned_article_id];

        var min = null, max = null;
        if(this.scanned_cabinet.products) {
            if(this.scanned_cabinet.products[this.scanned_article_id]) {
                min = this.scanned_cabinet.products[this.scanned_article_id].min;
                max = this.scanned_cabinet.products[this.scanned_article_id].max;
            }
        }

        var b = new Dialog('alert','','Volgende Stap',[
            {text:"Sluiten"},
            {text:"Scan volgende",filled:true,action:function(event) {
                app.scanArticle();
            }}
        ])
        var a = new Dialog('prompt',{
            labelName: app.scanned_article.name,
            inputAmount: {type:"number",label:"Aantal",value:'1',min:1,name:"largeNumberInput"},
            labelAmount: (min || max ? "Minimaal: "+min+", Maximaal: "+max : "")
        },"Dit artikel was al gescant",[
            {text:"Samenvoegen met huidige waarde ("+prevAmount+")",filled:true,action:function(event) {
                app.addArticleToList(parseInt(event.inputAmount)+parseInt(prevAmount));
            //    b.open();
            }},
            {text:"Overschrijven",filled:true,action:function(event) {
                app.addArticleToList(event.inputAmount);
            //    b.open();
            }},
            {text:"Annuleren"}
        ]);
        a.open();
    },
    addArticleToList: function(amount) {
        var articleID = this.scanned_article_id;
        console.log("Added article",articleID,"to list.");
        this.currentCabinetList[articleID] = amount;
        this.updateArticleList();
    },
    removeArticleFromList: function() {
        var articleID = this.scanned_article_id;
        delete(this.currentCabinetList[articleID]);
        console.log("Removed article",articleID,"from list.");
        this.updateArticleList();
    },
    editArticle: function(id) {
        console.log(id, this);
        if(typeof(id) !== "string" && this.nodeName) { // try loading it from the DOM
            var target = this;
            console.log(target);
            while(target.className != "articleDisplay") {target = target.parentNode;if(target == document.body) {return;}}
            id = target.getAttribute('data-id');
            console.log(id);
        } else if(typeof(id) !== "string") {
            return;
        }
        app.scanned_article = app.searchArticles(id);
        app.scanned_article_id = id;
        var currentAmount = app.currentCabinetList[id];
        var a = new Dialog("prompt",{
            inputAmount: {type:"number",label:"Aantal",value:currentAmount,min:1,name:"largeNumberInput"}
        },app.scanned_article.name,[
            {text:"Klaar",filled:true,action:function(event){
                app.addArticleToList(event.inputAmount);
            }},
            {text:"Verwijderen",action:function(event) {
                app.removeArticleFromList();
            }},
            {text:"Annuleren"}
        ]);
        a.open();
    },
    updateArticleList: function() {
        localforage.setItem('open_article',{currentCabinetList:this.currentCabinetList,scanned_cabinet:this.scanned_cabinet,scanned_cabinet_id:this.scanned_cabinet_id});
        for(var id in this.currentCabinetList) {
            var article = this.searchArticles(id);
            if(!article) {
                console.warn("Unable to find article ID ",id,article);
            } else {
                if(!elements.visible_articles[id]) {
                    // item doesn't exist yet, make a new one.
                    var item = {};
                    item.parent = document.createElement('div','articleDisplay');
                    item.parent.setAttribute('data-id',id);
                    item.parent.addEventListener('click',app.editArticle);
                    item.image = document.createElement('img',null,null,item.parent);
                    item.image.src = app.addresses[app.appMode].image+id;
                    item.image.onerror = function(event) {
                        console.log(event);
                        event.target.src = 'img/unknown_article.png';
                    }
                    item.title = document.createElement('span','articleTitle',null,item.parent);
                    item.subTitle = document.createElement('span','articleSubtitle',null,item.parent);
                    elements.scannedArticlesContainer.appendChild(item.parent);
                    elements.visible_articles[id] = item;
                } 
                // Update it's values
                elements.visible_articles[id].title.innerHTML = article.name;
                elements.visible_articles[id].subTitle.innerHTML = "Aantal: "+this.currentCabinetList[id];
                //html += "<tr onClick=\"app.editArticle('"+id+"')\"><th>"+this.currentCabinetList[id]+"</th><th>"+article.name+"</td><td class='material-icons'>edit</td></tr>";

            }
        }
        for(var id in elements.visible_articles) {
            if(!this.currentCabinetList[id]) {
                // remove this item;
                elements.scannedArticlesContainer.removeChild(elements.visible_articles[id].parent);
                delete(elements.visible_articles[id]);
            }
        }
    },
    finishCabinet: function() {
        if(count(app.currentCabinetList) == 0) {
            var d = new Dialog('alert','Er zijn geen artikelen gescant. Order verwijderen?','Waarschuwing',[
                {text:"terug"},
                {text:"Verwijderen",action:function() {
                    app.resetScannedItems();
                    var a = new Dialog('alert',"De order is verwijderd.");
                    a.open();
                }}
            ]);
        } else if(this.scanned_cabinet.requestpo == 1) {
            var d = new Dialog(
                'prompt',
                {
                    "msg_confirm":"Weet u zeker dat u deze order wilt versturen?<br><br>",
                    "poNumber": {label:"PO Nummer",value:this.scanned_cabinet.ponumber,required:true}
                },
                "Order Bevestigen?",
                [
                    {text:"Terug"},
                    {text:"Verwijderen",action:function(event) {
                        app.resetScannedItems();
                        var a = new Dialog('alert',"De order is verwijderd.");
                        a.open();
                    }},
                    {text:"Versturen",filled:true,requiresResults:true,action:function(event) {
                        console.log(event);
                        if(!event.poNumber || event.poNumber.trim() == "") {
                            new Dialog('alert','Ongeldig PO Nummer','Waarschuwing').open();
                            return;
                        }
                        var cabinetID = app.scanned_cabinet_id;
                        app.addItemToBacklog({
                            cabinetID: cabinetID,
                            apiKey: app.apiKey,
                            timestamp: Math.floor(new Date().getTime() / 1000),
                            articles: copyObject(app.currentCabinetList),
                            ponumber: event.poNumber,
                            version: version,
                            env: app.appMode
                        });
                    }}
                ]
            );
        }
        else {
            var d = new Dialog('alert',"Weet u zeker dat u deze order wilt versturen?","Order Bevestigen?",[
                {text:"Terug"},
                {text:"Verwijderen",action:function(event) {
                    app.resetScannedItems();
                    var a = new Dialog('alert',"De order is verwijderd.");
                    a.open();
                }},
                {text:"Versturen",filled:true,action:function(event) {
                    var cabinetID = app.scanned_cabinet_id;
                    app.addItemToBacklog({
                        cabinetID: cabinetID,
                        apiKey: app.apiKey,
                        timestamp: Math.floor(new Date().getTime() / 1000),
                        articles: copyObject(app.currentCabinetList),
                        version: version,
                        env: app.appMode
                    });
                }}
            ]);
        }
        d.open();
    },
    addItemToBacklog: function(data) {
        this.cabinetBacklog.push(data);
        localforage.setItem('cabinetBacklog',this.cabinetBacklog);
        console.log("Added items for cabinet",data.cabinetID,"to backlog.");
        if(!system.isOnline) {
            new Dialog('alert','Dit apparaat heeft geen verbinding met het internet. Deze order wordt pas verzonden zodra het met het internet verbonden is.','Let op!').open();
        }
        elements.sideNav.setActiveList(0,true);
        this.submitOrderBacklog();
        this.resetScannedItems();
    },
    resetScannedItems: function() {
        localforage.removeItem('open_article');
        app.scanned_article = null;
        app.scanned_article_id = null;
        app.scanned_cabinet = null;
        app.scanned_cabinet_id = null;
        this.currentCabinetList = {};
        app.showSpecificPage(elements.homePage);
        elements.cabinetPageContent.className = "content hiding";
        setTimeout(function() {
            elements.cabinetPage.removeAttribute('style');
        },500);
    },
    submitOrderBacklog: function() {
        console.log("Attempting to transmit cabinet backlog.");
        if(count(this.cabinetBacklog) == 0) {return;} //there's nothing to submit.

        var xhr = new XMLHttpRequest();
        xhr.open("POST",this.addresses[this.appMode].submitOrder,true);
        xhr.setRequestHeader("Content-Type","application/json");
        xhr.responseType = "json";
        xhr.onload = function(event) {
            var resp = xhr.response;
            var code = xhr.status;
            if(typeof(resp) == "string") {try {resp = JSON.parse(xhr.responseText);} catch(ex) {} }
            console.log(resp,code);
            if(resp && typeof(resp) == "object") {
                var newLog = []; // create the new log list
                for(var i = 0; i < app.cabinetBacklog.length; i++) {
                    var ok = false;
                    if(resp[i] && typeof(resp[i]) == "object") {
                        if(resp[i].status) {  // status is ok, it's safe to remove this entry
                           ok=true;
                           new Dialog('alert','',app.orderCodes[resp[i].reasoncode]).open();
                        }
                    }
                    if(!ok) { // status is not okay or something else is wrong. Keep the current item in the new array.
                        new Dialog("alert",(resp[i] && resp[i].reasoncode !== undefined ? app.orderCodes[resp[i].reasoncode] : "Kan order niet opslaan"),"Fout").open();
                        newLog.push(app.cabinetBacklog[i]);
                    }
                }
                app.cabinetBacklog = newLog; // overwrite the old log with the new log. If everything went right, this should be empty ([])
                if(app.cabinetBacklog.length == 0) {
                    elements.sideNav.setActiveList(0,false);
                }
                localforage.setItem('cabinetBacklog',app.cabinetBacklog);
            } // if resp isn't set or it's not a object, do nothing (assume api error)
        }
        // don't add error or timeout codes, since we're keeping the log until a successfull response came through.
        xhr.send(JSON.stringify(app.cabinetBacklog));
    },
    showCabinetBacklog: function() {
        if(this.cabinetBacklog.length) {
            var c = "";
            for(var i = 0; i < this.cabinetBacklog.length; i++) {
                var cN = (this.cabinets[this.cabinetBacklog[i].cabinetID] ? this.cabinets[this.cabinetBacklog[i].cabinetID].name : "Onbekend");
                var created = new Date(this.cabinetBacklog[i].timestamp*1000);
                c += "Kastnaam: "+cN+"<br>Aangemaakt: "+created.getDate()+"-"+(created.getMonth()+1)+"-"+created.getFullYear()+"<br><hr>";
            }
            new Dialog('alert',c,"Onverstuurde Orders",[
                {text:"Sluiten"},
                {text:"Nu versturen",filled:true,action:function(){
                    app.submitOrderBacklog();
                }}
            ]).open();
        }
    },
    startScan: function(msg_str, callback) {
        app.isScanning = true;
        var settings = this.scannerSettings;
        var callbackFunction = function(data) {
            window.setTimeout(function() {app.isScanning = false;}, 250); // reset the variable 1/4 s after coming back, due to backbutton being trigger happy
            callback(data);
        }
        if(msg_str) {settings.prompt = msg_str;}
        //plugins.GMVBarcodeScanner.scan(settings,callbackFunction);
        cordova.plugins.barcodeScanner.scan(callbackFunction,callbackFunction,settings);
        //cloudSky.zBar.scan(settings,callbackFunction,callbackFunction);
    },
};
if(window.system) {
    system.initialize();
}