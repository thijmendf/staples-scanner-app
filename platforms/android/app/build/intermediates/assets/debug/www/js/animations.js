/**
 * Creates a new instance of Loader, which is a circle animation based upon the Google's loader icon
 * @param {HTMLCanvasElement} [element] Optional Canvas node, if not given it'll create one itself
 */
var loader = function(element) {
	this.width = 80;
	this.height = 80;
	this.visible = false;
	if(!element || element.nodeName !== "CANVAS") {this.element = document.createElement('canvas');}
	else {this.element = element;}
	this.element.className += " loader";
	this.loaderColor = "grey";
	this.element.width = this.width;
	this.element.height= this.height;
	this.g = this.element.getContext('2d');
	this.prec = 0;
	this.frame = 0;
}
/**
 * Returns the canvas node
 * @returns {HTMLCanvasElement}
 */
loader.prototype.getElement = function() {
	return this.element;
}
/**
 * Size (w & h) of the loader canvas
 * @param {number} size 
 */
loader.prototype.setSize = function(size) {
	if(!size) {return false;}
	this.width = parseInt(size);
	this.height = parseInt(size);
	this.element.width = this.width;
	this.element.height= this.height;
}
/**
 * Renders a frame of the loader, and calls itself again
 */
loader.prototype.draw = function() {
	function a(f,p) {
		var t = (f/2) / p;
		return (--t)*t*t+1 // Ease out cubic
	}
	if(!this.g) {return;}
	if(!this.visible) {return;}
	this.g.clearRect(0,0,this.width,this.height);
	this.g.fillStyle=this.loaderColor;
	this.frame+=1.2;
	if(this.frame >= 100) {this.frame = 0;}
	this.prec += 0.05; // Rotation speed.   - Positive numbers: turn clockwise. Negative numbers: anti-clockwise.
	//if(this.prec > Math.PI*16) {this.prec-= Math.PI*16;} // Reset back to 0 after 4 spins (since we devide this by 4 at one point, else we could've made this 2)
	//else if(this.prec < -Math.PI*16) {this.prec+= Math.PI*16;} // Reset back to 0 after 4 spins (since we devide this by 4 at one point, else we could've made this 2)
	this.g.beginPath();
	this.g.moveTo(this.width/2,this.height/2);
	var s = 0;
	var e = 0;
	if(this.frame < 50) {
		s = this.frame / 50 + 0.5;
		e = a(this.frame,25) * Math.PI* (Math.PI - 0.5) / Math.PI * 2 + 1;
	} else {
		s = a(this.frame-50,25) * Math.PI * (Math.PI - 0.5) / Math.PI * 2 + 1.5;
		e = (this.frame - 50) / 50;
	}
	s += this.prec;
	e += this.prec;
	this.g.arc(this.width/2,this.height/2,this.width/2,-Math.PI/2 + s,-Math.PI/2 + e);
	this.g.lineTo(this.width/2,this.height/2);
	this.g.fill();
	this.g.save();
	this.g.globalCompositeOperation = "destination-out"; // delete old canvas where new canvas content is drawn.
	this.g.beginPath();
	this.g.arc(this.width/2,this.height/2,this.width/10*4,0,Math.PI*2);
	this.g.fill();
	this.g.restore();
	window.requestAnimationFrame(this.draw.bind(this));
}
/**
 * Starts the animation
 */
loader.prototype.start = function() {
	if(this.visible) {return false;}
	this.visible = true;
	this.draw();
}
/**
 * Stops the animation
 */
loader.prototype.stop = function() {
	this.visible = false;
}
/**
 * Sets the color of the circle
 * @param {string} clr color
 */
loader.prototype.setColor = function(clr) {
	this.loaderColor = clr;
}
/**
 * toggle its display to be visible
 */
loader.prototype.show = function() {
	this.element.style.display = "";
}
/**
 * toggle its display to be invisible
 */
loader.prototype.hide = function() {
	this.element.style.display = "none";
}

/**
 * Creates a instance of 'circle' which is a circle that draws from Start to End
 * @param {number} [start] a number displaying the value it starts from (defaults to 0)
 * @param {number} [end] a number determening the end value (defaults to 100) 
 * @param {number} [duration] Number of frames to draw the animation in (defaults to 60)
 */
var circle = function(start,end,duration) {
	this.color = "grey";
	this.width = 80;
	this.height = 80;
	this.thickness = 20;
	this.visible = true;
	
	this.tick = 0;
	this.total_frames = 60; // 60 FPS = 1 second.
	
	if(start === undefined) {
		this.startValue = 0;
	} else {
		this.startValue = parseFloat(start);
	}
	if(end === undefined) {
		this.endValue = 100;
	} else {
		this.endValue = parseFloat(end);
	}
	if(duration === undefined) {
		this.total_frames = 60;
	} else {
		this.total_frames = parseInt(duration);
	}
	this.value = this.startValue;
	
	this.element = document.createElement('canvas');
	this.element.className = "circle";
	this.element.width = this.width;
	this.element.height = this.height;
	this.g = this.element.getContext('2d');
	this.g.fillStyle = this.color;
	this.g.imageSmoothingEnabled = false;
	
	this.p = function(t, b, c, d) {
		t /= d;
		t--;
		return -c * (t*t*t*t - 1) + b;
	};
}
/**
 * sets the end value
 * @param {number} value 
 */
circle.prototype.setEndValue = function(value) {
	this.endValue = parseFloat(value);
}
/**
 * sets the start value
 * @param {number} value 
 */
circle.prototype.setStartValue = function(value) {
	this.startValue = parseFloat(value);
}
/**
 * starts the animation
 */
circle.prototype.start = function() {
	if(isNaN(this.endValue) || this.endValue == null) {
		return false;
	}
	this.value = this.startValue;
	this.tick = 0;
	this._draw();
}
/** internal function that renders the current frame and stops when it reached the end value */
circle.prototype._draw = function() {
	this.value = Math.round(this.p(this.tick,this.startValue,this.endValue-this.startValue,this.total_frames)*10)/10;
	if(this.visible) {
		this.g.clearRect(0,0,this.width,this.height);
		this.g.beginPath();
		this.g.moveTo(this.width/2,this.height/2);
		this.g.arc(this.width/2,this.height/2,this.width/2-4,-Math.PI/2,-(Math.PI/2)+Math.PI*2/100*this.value);
		this.g.lineTo(this.width/2,this.height/2);
		this.g.fill();
		this.g.save();
		this.g.globalCompositeOperation = "destination-out";
		this.g.beginPath();
		this.g.arc(this.width/2,this.height/2,this.width/2 - 4 - this.thickness,0,Math.PI*2);
		this.g.fill();
		this.g.restore();
	}
	if(this.tick < this.total_frames) {
		this.tick++;
		window.requestAnimationFrame(this._draw.bind(this));
	}
}
/**
 * clears the frame
 */
circle.prototype.clear = function() {
	this.g.clearRect(0,0,this.width,this.height);
}
/**
 * makes the circle invisible
 */
circle.prototype.hide = function() {
	this.visible = false;
	this.element.setAttribute('visible',0);
}
/**
 * shows the circle again
 */
circle.prototype.show = function() {
	this.visible = true;
	this.element.setAttribute('visible',1);
}

/**
 * creates a new instance of Progress, which is a progress bar that continiously loads
 */
var progress = function() {
	this.element = document.createElement('canvas');
	this.element.className = "progressbar";
	this.g = this.element.getContext('2d');
	
	this.width = Math.max(window.innerWidth,window.innerHeight);
	this.height = 1;
	this.element.height = this.height;
	this.element.width = this.width;
	
	this.tick = 0;
	this.max_ticks = 180; // at 60FPS
	this.offl = 0;
	this.visible = false;
	this.clear_color = "#DCEDC8";
	this.fill_color = "#8BC34A";
	this.fill_color = "#FF9800";
	
	this.easeInOutCubic = function (t, b, c, d) {
		t /= d/2;
		if (t < 1) return c/2*t*t*t + b;
		t -= 2;
		return c/2*(t*t*t + 2) + b;
	};
}
/**
 * internal function that renders the current frame
 */
progress.prototype._draw = function() {
	if(!this.visible) {return;}
	this.g.fillStyle = this.clear_color;
	this.g.fillRect(0,0,this.width,this.height);
	this.g.fillStyle= this.fill_color;
	
	var elw = this.easeInOutCubic(this.tick,0,this.width*2+this.width/3,this.max_ticks);
	var elw2 = elw;
	this.g.fillRect(elw2*1.4,0,elw*-0.4 - this.width/3,this.height);
	if(elw2 > this.width) {
		elw2 -= this.width;
		elw -= elw2*2.5;
		this.g.fillRect(elw2,0,elw*-0.4 - this.width/3,this.height);
	}
	
	this.tick++;
	if(this.tick > this.max_ticks * 0.75) {this.tick -= this.max_ticks * 0.75;}
	window.requestAnimationFrame(this._draw.bind(this));
}
/**
 * Starts the animation
 */
progress.prototype.start = function() {
	if(!this.visible) {
		this.visible = true;
		this.element.setAttribute('visible','1');
		this.tick = 0;
		this.offl = 0;
		this._draw();
	}
}
/**
 * Stops the animation
 */
progress.prototype.stop = function() {
	this.visible = false;
	this.element.removeAttribute('visible');
}