"use strict";

/*
console.oldwarn = console.warn;
console.warn = function() {
	for(var x = 0; x < arguments.length; x++) {
		alert(arguments[x]);
	}
}
console.error = console.warn;
*/
/**
 * Window.performance polyfill for devices that do not yet support it.
 */
if(window.performance) {
	if(!window.performance.now) {
		window.performance.start = new Date().getTime();
		window.performance.__proto__.now = function() {
			return new Date().getTime() - performance.start;
		}
	}
}
if(!window.requestAnimationFrame) {
	/**
	 * Polyfill function for devices that do not support requestAnimationFrame
	 * @param {function} callback 
	 */
	window.requestAnimationFrame = function(callback) {
		window.setTimeout(callback,16);
	}
}
/**
 * System library for device events.
 * These include initialize, deviceready, bindevents
 * recievedevent, pause, resume, ready and
 * event handlers for various events.
*/
var system = {
	isPaused: false,
	hasNavBar: false,
	realWidth: window.innerWidth,
	realHeight: window.innerHeight,
	densityB: "hdpi", // default
	densityV: 1.5,
	rotatesNavBar: false,
	isOnline: true,
	orientationchangeEvents: [],
	/**
	 * First function to be called when the app is started.
	 */
	initialize: function() {
		console.log("[system.initialize]","App Init");
		system.bindEvents();
	},
	/**
	 * Second function to be called when the app is started.
	 * Adds event listener for deviceready
	 */
	bindEvents: function() {
		console.log("[system.bindEvents]","Binding Events");
		document.addEventListener('deviceready', system.onDeviceReady, window.supportsPassive ? {passive:true} : false);
	},
	/**
	 * Function called when the deviceready event occours.  
	 * It checks for various window variables, and displays an error message if something is off.
	 * 
	 * Sets up the system.splashLoader
	 * 
	 * Checks the current WiFi SSID for Tolsma-Guest. The App will not work while connected to this network.
	 * 
	 * Loads information like screen size and density and other device metrics, software versions etc.
	 * This is mainly for the Android NavBar support.
	 * 
	 * Adds various event listeners to device events, like pause and resume, volume buttons etc.
	 * 
	 * Calls language.init at the end, which in turn calls sytem.receivedEvent(deviceready)
	 */
	onDeviceReady: function() {
		console.log("[system.onDeviceReady]","Device Ready");
		// First, check all libraries and objects to see if they're all running correctly!
		var ok = true;
		if(!window.cordova) {ok = false;}
	//	if(!window.cordova.InAppBrowser) {ok = false;}
		if(!window.StatusBar) {ok = false;}
	//	if(!window.FCMPlugin) {ok = false;}
	//	if(!window.localforage) {ok = false;}
	//	if(!navigator.Backbutton) {ok = false;}
		if(!window.screen) {ok = false;}
	//	if(!window.plugins) {ok = false;}
	//	if(!window.plugins.screensize) {ok = false;}
		
	//	if(!window.elements) {ok = false;}
	//	if(!window.app) {ok = false;}
	//	if(!window.lang) {ok = false;}
	//	if(!window.account) {ok = false;}
	//	if(!window.notifications) {ok = false;}
	//	if(!window.Regelstatus) {ok = false;}
		if(!window.loader) {ok = false;}
		if(!window.device) {ok = false;}
		
		if(!ok) {
			return alert("Error while starting. Please re-install the app!");
		}
	//	system.splashLoader = new loader(document.getElementById('splashLoader'));
	//	system.splashLoader.show();
	//	system.splashLoader.start();
		
	//	StatusBar.hide();

	//	if(window.WifiWizard) {
	//		WifiWizard.getCurrentSSID(function(result) {
	//			console.log(result);
	//			if(result == '"Tolsma-Guest"') {
	//				var a = new Dialog('alert','You are connected to Tolsma Guest. This wifi network will not work with the Tolsma App',lang.translate('warning'));
	//				a.open();
	//			}
	//		},function(error) {
	//			console.warn('Wifi Wizard',error);
	//		});
	//	}


		if(cordova.platformId == 'android') {
			navigator.app.overrideButton("menubutton", true);
			/*
			plugins.screensize.get(function(ss) {
				system.realWidth = ss.width;
				system.realHeight = ss.height;
				system.densityB = ss.densityBucket;
				system.densityV = ss.densityValue;
			});
			if(StatusBar.getAndroidVersion) {
				StatusBar.getAndroidVersion(function(result) {
					system.os_version = result.release;
					system.api_version = parseInt(result.sdk_int);
					if(system.api_version == null) {system.api_version = 28;} // temporary fix for Android P (which should be API version 28).
					system.model = result.model;
					system.manufacturer = result.manufacturer;
					
					if(system.hasNavBar) {
						if(system.api_version >= 24) {
							// Status bar rotates with the user since 7.0, but it doesn't do that on some screen sizes. If the isTablet plugin returns true, it means the Navigation Bar rotates.
							if(window.isTablet) {console.log("[system.onDeviceReady]",'nav bar rotates');system.rotatesNavBar = true; document.body.className += " NavBarRotates";}
						}
					}
					if(system.api_version <= 19) {
						document.body.setAttribute('disableStatusBar',true);
						app.statusBarHidden();
						StatusBar.show(); // There's no point coloring the status bar since it'll always be black.
						console.log("[system.onDeviceReady]","hiding NavBar");
						if(document.body) {document.body.className = document.body.className.replace("NavBarVisible","");}
						if(system.hasNavBar) {
							system.hasNavBar = false;
						}
					}
				});
			}
			*/
	//		StatusBar.backgroundColorByHexString("#689237");
			/*
			if(!StatusBar.hasNavBar) {return alert("Error - hasNavBar not here...");}
			StatusBar.hasNavBar(function(result){
				if(!result.hasMenuKey && !result.hasBackKey) { // NavBar visible
					if(window.screen.orientation) {document.body.setAttribute("orientation",screen.orientation.type);}
					if(system.api_version && system.api_version <= 19 || (device.isVirtual && system.densityV <= 1.5)) {return;}
					system.hasNavBar = true;
					setTimeout(function() {document.body.className += " NavBarVisible";});
					if(window.isTablet) {console.log("[system.onDeviceReady]",'nav bar rotates');system.rotatesNavBar = true; document.body.className += " NavBarRotates";}
					if(system.api_version >= 24) {
						// Status bar rotates with the user since 7.0, but it doesn't do that on some screen sizes. If the isTablet plugin returns true, it means the Navigation Bar rotates.
						// Futhermore, if it's NOT a tablet, it'll also appear left when the app is in landscape-secondary orientation
						if(!window.isTablet) {
							system.leftSideNavBar = true;
							setTimeout(function() {document.body.className += " leftSideNavBar";});
						}
					}
					var h = result.navBarHeight;
					h = h / system.densityV;
					system.navBarHeight = h;
					console.log("[system.onDeviceReady]","NavBar Height = ",h);
				}
			});
			*/
		} else {
			system.os_version = getOsVersion(window.navigator.appVersion,window.cordova.platformId);
		}
		
	//	window.open = cordova.InAppBrowser.open;
	//	if(!notifications.FCMBound) {
//			notifications.bindFCM();
//		}
		try {
			system.fixIphoneSupport();
		} catch(ex) {}
		
		
		document.body.setAttribute('platform',window.cordova.platformId);
		document.body.setAttribute('isTablet',window.isTablet);
		
		
		document.addEventListener('load',function(e) {console.log("Load",e);}, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('offline',function(e) {system.isOnline = false;console.log("Offline",e);}, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('online', function(e) {system.isOnline = true;app.submitOrderBacklog();console.log("Online",e);}, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('backbutton',system.backButton, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('menubutton', system.menubutton.bind(system), window.supportsPassive ? {passive:true} : false);
		document.addEventListener('searchbutton', function(e) {console.log("","searchbutton",e);}, window.supportsPassive ? {passive:true} : false);
	//	document.addEventListener('volumedownbutton', function(e) {console.log("","volumedownbutton",e);}, window.supportsPassive ? {passive:true} : false); // No need to capture these...
	//	document.addEventListener('volumeupbutton', function(e) {console.log("","volumeupbutton",e);}, window.supportsPassive ? {passive:true} : false);
	//	window.addEventListener('native.keyboardhide', function(e) {console.log("","onKeyboardShow",e);}, window.supportsPassive ? {passive:true} : false);
	//	window.addEventListener('native.keyboardshow', function(e) {console.log("","onKeyboardHide",e);}, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('pause',	system.pause, window.supportsPassive ? {passive:true} : false);
		document.addEventListener('resume', system.resume,window.supportsPassive ? {passive:true} : false);
		window.addEventListener('orientationchange', system.handleOrientationChange.bind(system), window.supportsPassive ? {passive:true} : false);
		

		system.receivedEvent('deviceready');

	},
	/**
	 * Function called after the languages are initialized correctly.
	 * 
	 * Calls for updatelanguage2, which eventually calls system.ready()
	 */
	receivedEvent: function(id) {
		console.log("[system.receivedEvent]","recieved event: ",id);
		if(id == "deviceready") {
			// Device (re)started
			system.ready();
		}
	},
	/**
	 * Function called if it's being run on an Iphone. it checks if the user uses an iPhone X. If true, it sets some attributes telling the browser to scale the viewport correctly
	 */
	fixIphoneSupport: function() {
		if(cordova.platformId == 'ios') {
		//	cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
			if(screen.orientation) {screen.orientation.isCustom = true;}
			if(window.orientation !== 0) {
				system.handleOrientationChange({});
			}
			var i = document.getElementsByName('viewport');
			if(i[0]) {
				i[0].setAttribute('content',i[0].getAttribute('content')+", viewport-fit=cover");
				var iPhoneNotchVersions = ['10','11'];
				if(iPhoneNotchVersions.indexOf(device.model.substr(6).substr(0,2)) !== -1) { // iPhone X, Xr, Xs etc.
					window.setTimeout(function() {document.body.setAttribute('iPhoneX',1);});
				}
			}
		}
	},
	/**
	 * Function that handles orientation changes, and calls any function in the orientationchangeEvents array.
	 * @param {event} e
	 */
	handleOrientationChange: function(e) {
		if(!screen.orientation || screen.orientation.isCustom) {
			screen.orientation = {angle:window.orientation,type:"portrait-primary",onchange:null,isCustom:true};
			switch(screen.orientation.angle) {
				case 0: screen.orientation.type = "portrait-primary";break;
				case 90: screen.orientation.type = "landscape-primary";break;
				case 180: screen.orientation.type = "portrait-secondary";break;
				case -90: screen.orientation.type = "landscape-secondary";break;
			}
		}
		console.log("[system.handleOrientationChange]","orientationchange",screen.orientation.type);
		document.body.setAttribute("orientation",screen.orientation.type);
		for(var i = 0; i < system.orientationchangeEvents.length; i++) {
			if(typeof(system.orientationchangeEvents[i]) == "function") {
				system.orientationchangeEvents[i](e);
			}
		}
	},
	/**
	 * Function that loads the screen size, DPI etc.  
	 * These results are not used right now.
	 */
	getScreenDPI: function() {
		var handler = function(result) {
			console.log("[system.getScreenDPI]",result);
			var dpi = null;
			var dp = 1;
			var multiplier = 1
			if(result.xdpi && result.ydpi) {
				dpi = Math.round((result.xdpi + result.ydpi) / 2);
			} else if(result.dpi) {
				dpi = result.dpi;
			} else if(result.scale) {
				
			}
			if(dpi != null) {
				dp = dpi / 160;
			}
		}
		window.plugins.screensize.get(handler, handler);
	},
	/**
	 * Function handling the Pause event. This is called when the app goes to the background, or the phone goes to sleep.
	 */
	pause: function(event) {
		system.isPaused = true;
		console.log("[system.pause]","Pause",event);
	},
	/**
	 * Function handling the Resume event. This is called when the app comes back to the foreground.
	 */
	resume: function(event) {
		system.isPaused = false;
		console.log("[system.resume]","Resume",event);
	},
	/**
	 * Function called when the language and device functions are finished.
	 * 
	 * It builds the HTML DOM, loads notification support and checks for session validity.
	 * @param {event} e
	 */
	ready: function(e) {
		system.isReady = true;
		var start = performance.now();
		try {
			elements.buildDom();
			if(system.api_version && system.api_version <= 18) { // Warn the user that this version is old AF
				var a = new Dialog('alert','warning_old_android' ,'warning');
				a.open();
				document.body.className += " compat";
			}
		} catch(ex) {
			console.error("[system.ready]",ex);
			return alert("Something went wrong while building elements. Please try restarting the app.\n"+ex);
		}
		var end = performance.now();
		console.log("[system.ready]","Building DOM took",(end-start),"ms");

		StatusBar.backgroundColorByHexString(app.statusBarColor);
		StatusBar.show();
		StatusBar.overlaysWebView(false);
		StatusBar.styleDefault();
		if(device.sdk <= 21) {
			StatusBar.backgroundColorByHexString('#3A3A39');
		}
		app.startup();
	},
	/**
	 * Function that handles the menubutton event.  
	 * it attempts to open the top right menu if the user is on such screen.
	 */
	menubutton: function(event) {
		console.log(event);
	},
	/**
	 * Function that handles the backbutton event. It closes windows etc. until the user is back on the homepage. If called again, it'll minimize the App.
	 */
	backButton: function(event) {
		var debug = false;
		event.preventDefault();
		if(app.isScanning) {return;}
		navigator.Backbutton.goBack(function(e) {console.log("[system.backButton]","Success",e);},function(e) {console.warn("[system.backButton]","Error",e);});
	},
	/*
	downloadImage: function(image,url) {
		var localuri = image.src;
		var xhr = new XMLHttpRequest();
		xhr.open("GET",url,true);
		xhr.responseType = "blob";
		xhr.onload = function() {
			if(xhr.status == 200) {
				system.saveFile(
					localuri,
					xhr.response,
					function(error,result) {
						if(result){ 
							image.src = localuri+"#"+new Date().getTime();
						} else {
							console.warn("[system.downloadImage]",error);
						}
					}
				);
			}
		}
		xhr.onerror = function() {
			console.warn("[system.downloadImage]","Unable to download image",url,localuri);
		}
		xhr.send();
	},
	getFileEntry: function(path,canCreate,callback) {
		if(path === undefined || canCreate === undefined || callback === undefined) {return false;}
		var paths = path.split("/");
		var file = paths[paths.length-1];
		delete(paths[paths.length-1]);
		var path = paths.join("/");
		console.log(paths,file,path);
		window.resolveLocalFileSystemURL(path,function(folder) {
			folder.getFile(file,{create:canCreate},function(fileEntry) {
				callback(null,fileEntry);
			}, function(error) {
				callback(error,null);
			});
		}, function(error) {
			if(error.code == 1 && canCreate) { // It's a 404 but we may create new folders and files. Let's go and do that.
				var rootfolder = paths[0]+"//"+paths[2]+"/"+paths[3]+"/";
				console.log(rootfolder);
				window.resolveLocalFileSystemURL(rootfolder,function(folder) {
					function getFolder(pfolder,path,index,cb) {
						console.log(pfolder,path,index);
						pfolder.getDirectory(path[index],{create:true},function(folderEntry) {
							if(path[index+1]) { // We got more folders to enter.
								getFolder(folderEntry,path,index+1,cb);
							} else {
								cb(null,folderEntry);
							}
						},function(error) {
							cb(error,null);
						});
					}
					// Ok, we got the root.
					getFolder(folder,paths,4,function(error,entry) {
						console.log(error,entry);
						if(entry) {
							entry.getFile(file,{create:canCreate},function(fileEntry) {
								callback(null,fileEntry);
							}, function(error) {
								callback(error,null);
							});
						} else {
							callback(error,null);
						}
					});
				},function(error) {
					console.log(error);
					callback(error,null);
				});
			} else {	
				callback(error,null);
			}
		});
	},
	saveFile: function(path,data,callback) {
		this.getFileEntry(path,true,function(error,fileEntry) {
			if(error) {
				callback(error,null);
			} else {
				system.writeFile(fileEntry,data,callback);
			}
		});
	},
	writeFile: function(fileEntry,dataObj,callback) {
		if(!dataObj) {return false;}
		fileEntry.createWriter(function (fileWriter) {
			fileWriter.onwriteend = function(event) {
				console.log("File wrote successfully",event);
				callback(null,true);
			};
			fileWriter.onerror = function (e) {
				console.error("Failed file write: " + e.toString());
				callback(e,null);
			};
			fileWriter.write(dataObj);
		});
	},
	deleteFile: function(properties,callback) {
		if(properties.fileEntry) {
			properties.fileEntry.remove(function(e) {
				// Deleted successfully
				callback(null,true);
			},function(e) {
				// Error deleting
				console.warn("2nd callback");
				callback(e,null);
			},function(e) {
				// 404?
				console.warn("3rd callback");
				callback(e,null);
			});
		} else if(properties.path) {
			this.getFileEntry(properties.path,false,function(error,fileEntry) {
				if(error === null) {
					system.deleteFile({fileEntry:fileEntry},callback);
				} else {
					console.log("Error");
					callback(error,null);
				}
			});
		} else {
			return false;
		}
	}*/
};